
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour fileinfoPK complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="fileinfoPK">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idcase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idmain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idpart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fileinfoPK", propOrder = {
    "idcase",
    "idfile",
    "idmain",
    "idpart"
})
public class FileinfoPK {

    protected String idcase;
    protected String idfile;
    protected String idmain;
    protected String idpart;

    /**
     * Obtient la valeur de la propriété idcase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdcase() {
        return idcase;
    }

    /**
     * Définit la valeur de la propriété idcase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdcase(String value) {
        this.idcase = value;
    }

    /**
     * Obtient la valeur de la propriété idfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfile() {
        return idfile;
    }

    /**
     * Définit la valeur de la propriété idfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfile(String value) {
        this.idfile = value;
    }

    /**
     * Obtient la valeur de la propriété idmain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdmain() {
        return idmain;
    }

    /**
     * Définit la valeur de la propriété idmain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdmain(String value) {
        this.idmain = value;
    }

    /**
     * Obtient la valeur de la propriété idpart.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdpart() {
        return idpart;
    }

    /**
     * Définit la valeur de la propriété idpart.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdpart(String value) {
        this.idpart = value;
    }

}
