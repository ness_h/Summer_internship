
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour form complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="form">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idmain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nameArb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "form", propOrder = {
    "idmain",
    "name",
    "nameArb"
})
public class Form {

    protected String idmain;
    protected String name;
    protected String nameArb;

    /**
     * Obtient la valeur de la propriété idmain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdmain() {
        return idmain;
    }

    /**
     * Définit la valeur de la propriété idmain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdmain(String value) {
        this.idmain = value;
    }

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété nameArb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameArb() {
        return nameArb;
    }

    /**
     * Définit la valeur de la propriété nameArb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameArb(String value) {
        this.nameArb = value;
    }

}
