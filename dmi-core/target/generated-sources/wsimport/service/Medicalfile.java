
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour medicalfile complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="medicalfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bloodpressure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bodytemperature" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnostic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idpatient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="painrate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pulse" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="respiratoryrate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "medicalfile", propOrder = {
    "bloodpressure",
    "bodytemperature",
    "date",
    "diagnostic",
    "idfile",
    "idpatient",
    "painrate",
    "pulse",
    "respiratoryrate",
    "weight"
})
public class Medicalfile {

    protected String bloodpressure;
    protected Integer bodytemperature;
    protected String date;
    protected String diagnostic;
    protected String idfile;
    protected String idpatient;
    protected Integer painrate;
    protected Integer pulse;
    protected Integer respiratoryrate;
    protected Integer weight;

    /**
     * Obtient la valeur de la propriété bloodpressure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBloodpressure() {
        return bloodpressure;
    }

    /**
     * Définit la valeur de la propriété bloodpressure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBloodpressure(String value) {
        this.bloodpressure = value;
    }

    /**
     * Obtient la valeur de la propriété bodytemperature.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBodytemperature() {
        return bodytemperature;
    }

    /**
     * Définit la valeur de la propriété bodytemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBodytemperature(Integer value) {
        this.bodytemperature = value;
    }

    /**
     * Obtient la valeur de la propriété date.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Obtient la valeur de la propriété diagnostic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiagnostic() {
        return diagnostic;
    }

    /**
     * Définit la valeur de la propriété diagnostic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiagnostic(String value) {
        this.diagnostic = value;
    }

    /**
     * Obtient la valeur de la propriété idfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfile() {
        return idfile;
    }

    /**
     * Définit la valeur de la propriété idfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfile(String value) {
        this.idfile = value;
    }

    /**
     * Obtient la valeur de la propriété idpatient.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdpatient() {
        return idpatient;
    }

    /**
     * Définit la valeur de la propriété idpatient.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdpatient(String value) {
        this.idpatient = value;
    }

    /**
     * Obtient la valeur de la propriété painrate.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPainrate() {
        return painrate;
    }

    /**
     * Définit la valeur de la propriété painrate.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPainrate(Integer value) {
        this.painrate = value;
    }

    /**
     * Obtient la valeur de la propriété pulse.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPulse() {
        return pulse;
    }

    /**
     * Définit la valeur de la propriété pulse.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPulse(Integer value) {
        this.pulse = value;
    }

    /**
     * Obtient la valeur de la propriété respiratoryrate.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRespiratoryrate() {
        return respiratoryrate;
    }

    /**
     * Définit la valeur de la propriété respiratoryrate.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRespiratoryrate(Integer value) {
        this.respiratoryrate = value;
    }

    /**
     * Obtient la valeur de la propriété weight.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * Définit la valeur de la propriété weight.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWeight(Integer value) {
        this.weight = value;
    }

}
