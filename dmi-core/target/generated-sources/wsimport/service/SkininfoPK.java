
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour skininfoPK complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="skininfoPK">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idskin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "skininfoPK", propOrder = {
    "idfile",
    "idskin"
})
public class SkininfoPK {

    protected String idfile;
    protected String idskin;

    /**
     * Obtient la valeur de la propriété idfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfile() {
        return idfile;
    }

    /**
     * Définit la valeur de la propriété idfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfile(String value) {
        this.idfile = value;
    }

    /**
     * Obtient la valeur de la propriété idskin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdskin() {
        return idskin;
    }

    /**
     * Définit la valeur de la propriété idskin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdskin(String value) {
        this.idskin = value;
    }

}
