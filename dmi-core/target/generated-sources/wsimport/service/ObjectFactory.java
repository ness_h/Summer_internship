
package service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TodayspatResponse_QNAME = new QName("http://service.dmi.csys.com/", "todayspatResponse");
    private final static QName _Patient_QNAME = new QName("http://service.dmi.csys.com/", "patient");
    private final static QName _Insertmf_QNAME = new QName("http://service.dmi.csys.com/", "insertmf");
    private final static QName _Getnamemain_QNAME = new QName("http://service.dmi.csys.com/", "getnamemain");
    private final static QName _GetlistfilesbyidpatientResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistfilesbyidpatientResponse");
    private final static QName _PainrateResponse_QNAME = new QName("http://service.dmi.csys.com/", "painrateResponse");
    private final static QName _Log_QNAME = new QName("http://service.dmi.csys.com/", "log");
    private final static QName _Insert_QNAME = new QName("http://service.dmi.csys.com/", "insert");
    private final static QName _Update_QNAME = new QName("http://service.dmi.csys.com/", "update");
    private final static QName _GetlistpatientResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistpatientResponse");
    private final static QName _GetlistfilesbyidfileResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistfilesbyidfileResponse");
    private final static QName _Getlistfilesbyidpatient_QNAME = new QName("http://service.dmi.csys.com/", "getlistfilesbyidpatient");
    private final static QName _GetlistskinResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistskinResponse");
    private final static QName _CheckuserResponse_QNAME = new QName("http://service.dmi.csys.com/", "checkuserResponse");
    private final static QName _FindnameforidResponse_QNAME = new QName("http://service.dmi.csys.com/", "findnameforidResponse");
    private final static QName _Setuser_QNAME = new QName("http://service.dmi.csys.com/", "setuser");
    private final static QName _Getusers_QNAME = new QName("http://service.dmi.csys.com/", "getusers");
    private final static QName _Form_QNAME = new QName("http://service.dmi.csys.com/", "form");
    private final static QName _Gettypecase_QNAME = new QName("http://service.dmi.csys.com/", "gettypecase");
    private final static QName _GetmainResponse_QNAME = new QName("http://service.dmi.csys.com/", "getmainResponse");
    private final static QName _GetusersResponse_QNAME = new QName("http://service.dmi.csys.com/", "getusersResponse");
    private final static QName _Formpart_QNAME = new QName("http://service.dmi.csys.com/", "formpart");
    private final static QName _Getlistfiles_QNAME = new QName("http://service.dmi.csys.com/", "getlistfiles");
    private final static QName _Skininfo_QNAME = new QName("http://service.dmi.csys.com/", "skininfo");
    private final static QName _Getmaxid_QNAME = new QName("http://service.dmi.csys.com/", "getmaxid");
    private final static QName _GetdateResponse_QNAME = new QName("http://service.dmi.csys.com/", "getdateResponse");
    private final static QName _Todayspat_QNAME = new QName("http://service.dmi.csys.com/", "todayspat");
    private final static QName _Getlistmain_QNAME = new QName("http://service.dmi.csys.com/", "getlistmain");
    private final static QName _FinduserResponse_QNAME = new QName("http://service.dmi.csys.com/", "finduserResponse");
    private final static QName _SetuserResponse_QNAME = new QName("http://service.dmi.csys.com/", "setuserResponse");
    private final static QName _GetnamemainResponse_QNAME = new QName("http://service.dmi.csys.com/", "getnamemainResponse");
    private final static QName _Findnameforid_QNAME = new QName("http://service.dmi.csys.com/", "findnameforid");
    private final static QName _Insertskin_QNAME = new QName("http://service.dmi.csys.com/", "insertskin");
    private final static QName _Getnamepart_QNAME = new QName("http://service.dmi.csys.com/", "getnamepart");
    private final static QName _Getlistfilesbyidfile_QNAME = new QName("http://service.dmi.csys.com/", "getlistfilesbyidfile");
    private final static QName _Finduser_QNAME = new QName("http://service.dmi.csys.com/", "finduser");
    private final static QName _Fileinfo_QNAME = new QName("http://service.dmi.csys.com/", "fileinfo");
    private final static QName _GetnamepartResponse_QNAME = new QName("http://service.dmi.csys.com/", "getnamepartResponse");
    private final static QName _InsertskinResponse_QNAME = new QName("http://service.dmi.csys.com/", "insertskinResponse");
    private final static QName _Getlistinfo_QNAME = new QName("http://service.dmi.csys.com/", "getlistinfo");
    private final static QName _GetnamecaseResponse_QNAME = new QName("http://service.dmi.csys.com/", "getnamecaseResponse");
    private final static QName _Getlistcase_QNAME = new QName("http://service.dmi.csys.com/", "getlistcase");
    private final static QName _Getmain_QNAME = new QName("http://service.dmi.csys.com/", "getmain");
    private final static QName _GetlistcaseResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistcaseResponse");
    private final static QName _Checkuser_QNAME = new QName("http://service.dmi.csys.com/", "checkuser");
    private final static QName _Getallcase_QNAME = new QName("http://service.dmi.csys.com/", "getallcase");
    private final static QName _Painrate_QNAME = new QName("http://service.dmi.csys.com/", "painrate");
    private final static QName _Getdate_QNAME = new QName("http://service.dmi.csys.com/", "getdate");
    private final static QName _GetlistpartResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistpartResponse");
    private final static QName _GetlistmainResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistmainResponse");
    private final static QName _InsertResponse_QNAME = new QName("http://service.dmi.csys.com/", "insertResponse");
    private final static QName _Getlistpatient_QNAME = new QName("http://service.dmi.csys.com/", "getlistpatient");
    private final static QName _Getlistskin_QNAME = new QName("http://service.dmi.csys.com/", "getlistskin");
    private final static QName _GetmaxidResponse_QNAME = new QName("http://service.dmi.csys.com/", "getmaxidResponse");
    private final static QName _InsertmfResponse_QNAME = new QName("http://service.dmi.csys.com/", "insertmfResponse");
    private final static QName _GetlistfilesResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistfilesResponse");
    private final static QName _Formcase_QNAME = new QName("http://service.dmi.csys.com/", "formcase");
    private final static QName _InsertfileResponse_QNAME = new QName("http://service.dmi.csys.com/", "insertfileResponse");
    private final static QName _Getallpart_QNAME = new QName("http://service.dmi.csys.com/", "getallpart");
    private final static QName _GetlistinfoResponse_QNAME = new QName("http://service.dmi.csys.com/", "getlistinfoResponse");
    private final static QName _UpdateResponse_QNAME = new QName("http://service.dmi.csys.com/", "updateResponse");
    private final static QName _GettypecaseResponse_QNAME = new QName("http://service.dmi.csys.com/", "gettypecaseResponse");
    private final static QName _Getlistpart_QNAME = new QName("http://service.dmi.csys.com/", "getlistpart");
    private final static QName _Insertfile_QNAME = new QName("http://service.dmi.csys.com/", "insertfile");
    private final static QName _Getnamecase_QNAME = new QName("http://service.dmi.csys.com/", "getnamecase");
    private final static QName _GetallpartResponse_QNAME = new QName("http://service.dmi.csys.com/", "getallpartResponse");
    private final static QName _Medicalfile_QNAME = new QName("http://service.dmi.csys.com/", "medicalfile");
    private final static QName _GetallcaseResponse_QNAME = new QName("http://service.dmi.csys.com/", "getallcaseResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Checkuser }
     * 
     */
    public Checkuser createCheckuser() {
        return new Checkuser();
    }

    /**
     * Create an instance of {@link GetlistcaseResponse }
     * 
     */
    public GetlistcaseResponse createGetlistcaseResponse() {
        return new GetlistcaseResponse();
    }

    /**
     * Create an instance of {@link Painrate }
     * 
     */
    public Painrate createPainrate() {
        return new Painrate();
    }

    /**
     * Create an instance of {@link Getallcase }
     * 
     */
    public Getallcase createGetallcase() {
        return new Getallcase();
    }

    /**
     * Create an instance of {@link Getdate }
     * 
     */
    public Getdate createGetdate() {
        return new Getdate();
    }

    /**
     * Create an instance of {@link GetlistpartResponse }
     * 
     */
    public GetlistpartResponse createGetlistpartResponse() {
        return new GetlistpartResponse();
    }

    /**
     * Create an instance of {@link GetlistmainResponse }
     * 
     */
    public GetlistmainResponse createGetlistmainResponse() {
        return new GetlistmainResponse();
    }

    /**
     * Create an instance of {@link InsertResponse }
     * 
     */
    public InsertResponse createInsertResponse() {
        return new InsertResponse();
    }

    /**
     * Create an instance of {@link Findnameforid }
     * 
     */
    public Findnameforid createFindnameforid() {
        return new Findnameforid();
    }

    /**
     * Create an instance of {@link Insertskin }
     * 
     */
    public Insertskin createInsertskin() {
        return new Insertskin();
    }

    /**
     * Create an instance of {@link GetnamemainResponse }
     * 
     */
    public GetnamemainResponse createGetnamemainResponse() {
        return new GetnamemainResponse();
    }

    /**
     * Create an instance of {@link Getlistfilesbyidfile }
     * 
     */
    public Getlistfilesbyidfile createGetlistfilesbyidfile() {
        return new Getlistfilesbyidfile();
    }

    /**
     * Create an instance of {@link Getnamepart }
     * 
     */
    public Getnamepart createGetnamepart() {
        return new Getnamepart();
    }

    /**
     * Create an instance of {@link Finduser }
     * 
     */
    public Finduser createFinduser() {
        return new Finduser();
    }

    /**
     * Create an instance of {@link Fileinfo }
     * 
     */
    public Fileinfo createFileinfo() {
        return new Fileinfo();
    }

    /**
     * Create an instance of {@link GetnamepartResponse }
     * 
     */
    public GetnamepartResponse createGetnamepartResponse() {
        return new GetnamepartResponse();
    }

    /**
     * Create an instance of {@link Getlistinfo }
     * 
     */
    public Getlistinfo createGetlistinfo() {
        return new Getlistinfo();
    }

    /**
     * Create an instance of {@link GetnamecaseResponse }
     * 
     */
    public GetnamecaseResponse createGetnamecaseResponse() {
        return new GetnamecaseResponse();
    }

    /**
     * Create an instance of {@link InsertskinResponse }
     * 
     */
    public InsertskinResponse createInsertskinResponse() {
        return new InsertskinResponse();
    }

    /**
     * Create an instance of {@link Getlistcase }
     * 
     */
    public Getlistcase createGetlistcase() {
        return new Getlistcase();
    }

    /**
     * Create an instance of {@link Getmain }
     * 
     */
    public Getmain createGetmain() {
        return new Getmain();
    }

    /**
     * Create an instance of {@link GettypecaseResponse }
     * 
     */
    public GettypecaseResponse createGettypecaseResponse() {
        return new GettypecaseResponse();
    }

    /**
     * Create an instance of {@link Getlistpart }
     * 
     */
    public Getlistpart createGetlistpart() {
        return new Getlistpart();
    }

    /**
     * Create an instance of {@link Getnamecase }
     * 
     */
    public Getnamecase createGetnamecase() {
        return new Getnamecase();
    }

    /**
     * Create an instance of {@link Insertfile }
     * 
     */
    public Insertfile createInsertfile() {
        return new Insertfile();
    }

    /**
     * Create an instance of {@link Medicalfile }
     * 
     */
    public Medicalfile createMedicalfile() {
        return new Medicalfile();
    }

    /**
     * Create an instance of {@link GetallpartResponse }
     * 
     */
    public GetallpartResponse createGetallpartResponse() {
        return new GetallpartResponse();
    }

    /**
     * Create an instance of {@link GetallcaseResponse }
     * 
     */
    public GetallcaseResponse createGetallcaseResponse() {
        return new GetallcaseResponse();
    }

    /**
     * Create an instance of {@link Getlistpatient }
     * 
     */
    public Getlistpatient createGetlistpatient() {
        return new Getlistpatient();
    }

    /**
     * Create an instance of {@link Getlistskin }
     * 
     */
    public Getlistskin createGetlistskin() {
        return new Getlistskin();
    }

    /**
     * Create an instance of {@link GetmaxidResponse }
     * 
     */
    public GetmaxidResponse createGetmaxidResponse() {
        return new GetmaxidResponse();
    }

    /**
     * Create an instance of {@link InsertmfResponse }
     * 
     */
    public InsertmfResponse createInsertmfResponse() {
        return new InsertmfResponse();
    }

    /**
     * Create an instance of {@link Formcase }
     * 
     */
    public Formcase createFormcase() {
        return new Formcase();
    }

    /**
     * Create an instance of {@link GetlistfilesResponse }
     * 
     */
    public GetlistfilesResponse createGetlistfilesResponse() {
        return new GetlistfilesResponse();
    }

    /**
     * Create an instance of {@link InsertfileResponse }
     * 
     */
    public InsertfileResponse createInsertfileResponse() {
        return new InsertfileResponse();
    }

    /**
     * Create an instance of {@link Getallpart }
     * 
     */
    public Getallpart createGetallpart() {
        return new Getallpart();
    }

    /**
     * Create an instance of {@link GetlistinfoResponse }
     * 
     */
    public GetlistinfoResponse createGetlistinfoResponse() {
        return new GetlistinfoResponse();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link GetlistfilesbyidpatientResponse }
     * 
     */
    public GetlistfilesbyidpatientResponse createGetlistfilesbyidpatientResponse() {
        return new GetlistfilesbyidpatientResponse();
    }

    /**
     * Create an instance of {@link PainrateResponse }
     * 
     */
    public PainrateResponse createPainrateResponse() {
        return new PainrateResponse();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link GetlistpatientResponse }
     * 
     */
    public GetlistpatientResponse createGetlistpatientResponse() {
        return new GetlistpatientResponse();
    }

    /**
     * Create an instance of {@link Insert }
     * 
     */
    public Insert createInsert() {
        return new Insert();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link CheckuserResponse }
     * 
     */
    public CheckuserResponse createCheckuserResponse() {
        return new CheckuserResponse();
    }

    /**
     * Create an instance of {@link GetlistfilesbyidfileResponse }
     * 
     */
    public GetlistfilesbyidfileResponse createGetlistfilesbyidfileResponse() {
        return new GetlistfilesbyidfileResponse();
    }

    /**
     * Create an instance of {@link Getlistfilesbyidpatient }
     * 
     */
    public Getlistfilesbyidpatient createGetlistfilesbyidpatient() {
        return new Getlistfilesbyidpatient();
    }

    /**
     * Create an instance of {@link GetlistskinResponse }
     * 
     */
    public GetlistskinResponse createGetlistskinResponse() {
        return new GetlistskinResponse();
    }

    /**
     * Create an instance of {@link TodayspatResponse }
     * 
     */
    public TodayspatResponse createTodayspatResponse() {
        return new TodayspatResponse();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link Insertmf }
     * 
     */
    public Insertmf createInsertmf() {
        return new Insertmf();
    }

    /**
     * Create an instance of {@link Getnamemain }
     * 
     */
    public Getnamemain createGetnamemain() {
        return new Getnamemain();
    }

    /**
     * Create an instance of {@link Getlistfiles }
     * 
     */
    public Getlistfiles createGetlistfiles() {
        return new Getlistfiles();
    }

    /**
     * Create an instance of {@link Skininfo }
     * 
     */
    public Skininfo createSkininfo() {
        return new Skininfo();
    }

    /**
     * Create an instance of {@link Getmaxid }
     * 
     */
    public Getmaxid createGetmaxid() {
        return new Getmaxid();
    }

    /**
     * Create an instance of {@link Todayspat }
     * 
     */
    public Todayspat createTodayspat() {
        return new Todayspat();
    }

    /**
     * Create an instance of {@link GetdateResponse }
     * 
     */
    public GetdateResponse createGetdateResponse() {
        return new GetdateResponse();
    }

    /**
     * Create an instance of {@link FinduserResponse }
     * 
     */
    public FinduserResponse createFinduserResponse() {
        return new FinduserResponse();
    }

    /**
     * Create an instance of {@link Getlistmain }
     * 
     */
    public Getlistmain createGetlistmain() {
        return new Getlistmain();
    }

    /**
     * Create an instance of {@link SetuserResponse }
     * 
     */
    public SetuserResponse createSetuserResponse() {
        return new SetuserResponse();
    }

    /**
     * Create an instance of {@link FindnameforidResponse }
     * 
     */
    public FindnameforidResponse createFindnameforidResponse() {
        return new FindnameforidResponse();
    }

    /**
     * Create an instance of {@link Setuser }
     * 
     */
    public Setuser createSetuser() {
        return new Setuser();
    }

    /**
     * Create an instance of {@link Form }
     * 
     */
    public Form createForm() {
        return new Form();
    }

    /**
     * Create an instance of {@link Getusers }
     * 
     */
    public Getusers createGetusers() {
        return new Getusers();
    }

    /**
     * Create an instance of {@link Gettypecase }
     * 
     */
    public Gettypecase createGettypecase() {
        return new Gettypecase();
    }

    /**
     * Create an instance of {@link GetmainResponse }
     * 
     */
    public GetmainResponse createGetmainResponse() {
        return new GetmainResponse();
    }

    /**
     * Create an instance of {@link GetusersResponse }
     * 
     */
    public GetusersResponse createGetusersResponse() {
        return new GetusersResponse();
    }

    /**
     * Create an instance of {@link Formpart }
     * 
     */
    public Formpart createFormpart() {
        return new Formpart();
    }

    /**
     * Create an instance of {@link SkininfoPK }
     * 
     */
    public SkininfoPK createSkininfoPK() {
        return new SkininfoPK();
    }

    /**
     * Create an instance of {@link FileinfoPK }
     * 
     */
    public FileinfoPK createFileinfoPK() {
        return new FileinfoPK();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TodayspatResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "todayspatResponse")
    public JAXBElement<TodayspatResponse> createTodayspatResponse(TodayspatResponse value) {
        return new JAXBElement<TodayspatResponse>(_TodayspatResponse_QNAME, TodayspatResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Patient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "patient")
    public JAXBElement<Patient> createPatient(Patient value) {
        return new JAXBElement<Patient>(_Patient_QNAME, Patient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insertmf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertmf")
    public JAXBElement<Insertmf> createInsertmf(Insertmf value) {
        return new JAXBElement<Insertmf>(_Insertmf_QNAME, Insertmf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getnamemain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamemain")
    public JAXBElement<Getnamemain> createGetnamemain(Getnamemain value) {
        return new JAXBElement<Getnamemain>(_Getnamemain_QNAME, Getnamemain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistfilesbyidpatientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfilesbyidpatientResponse")
    public JAXBElement<GetlistfilesbyidpatientResponse> createGetlistfilesbyidpatientResponse(GetlistfilesbyidpatientResponse value) {
        return new JAXBElement<GetlistfilesbyidpatientResponse>(_GetlistfilesbyidpatientResponse_QNAME, GetlistfilesbyidpatientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PainrateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "painrateResponse")
    public JAXBElement<PainrateResponse> createPainrateResponse(PainrateResponse value) {
        return new JAXBElement<PainrateResponse>(_PainrateResponse_QNAME, PainrateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Log }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "log")
    public JAXBElement<Log> createLog(Log value) {
        return new JAXBElement<Log>(_Log_QNAME, Log.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insert }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insert")
    public JAXBElement<Insert> createInsert(Insert value) {
        return new JAXBElement<Insert>(_Insert_QNAME, Insert.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Update }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "update")
    public JAXBElement<Update> createUpdate(Update value) {
        return new JAXBElement<Update>(_Update_QNAME, Update.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistpatientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistpatientResponse")
    public JAXBElement<GetlistpatientResponse> createGetlistpatientResponse(GetlistpatientResponse value) {
        return new JAXBElement<GetlistpatientResponse>(_GetlistpatientResponse_QNAME, GetlistpatientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistfilesbyidfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfilesbyidfileResponse")
    public JAXBElement<GetlistfilesbyidfileResponse> createGetlistfilesbyidfileResponse(GetlistfilesbyidfileResponse value) {
        return new JAXBElement<GetlistfilesbyidfileResponse>(_GetlistfilesbyidfileResponse_QNAME, GetlistfilesbyidfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistfilesbyidpatient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfilesbyidpatient")
    public JAXBElement<Getlistfilesbyidpatient> createGetlistfilesbyidpatient(Getlistfilesbyidpatient value) {
        return new JAXBElement<Getlistfilesbyidpatient>(_Getlistfilesbyidpatient_QNAME, Getlistfilesbyidpatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistskinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistskinResponse")
    public JAXBElement<GetlistskinResponse> createGetlistskinResponse(GetlistskinResponse value) {
        return new JAXBElement<GetlistskinResponse>(_GetlistskinResponse_QNAME, GetlistskinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckuserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "checkuserResponse")
    public JAXBElement<CheckuserResponse> createCheckuserResponse(CheckuserResponse value) {
        return new JAXBElement<CheckuserResponse>(_CheckuserResponse_QNAME, CheckuserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindnameforidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "findnameforidResponse")
    public JAXBElement<FindnameforidResponse> createFindnameforidResponse(FindnameforidResponse value) {
        return new JAXBElement<FindnameforidResponse>(_FindnameforidResponse_QNAME, FindnameforidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Setuser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "setuser")
    public JAXBElement<Setuser> createSetuser(Setuser value) {
        return new JAXBElement<Setuser>(_Setuser_QNAME, Setuser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getusers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getusers")
    public JAXBElement<Getusers> createGetusers(Getusers value) {
        return new JAXBElement<Getusers>(_Getusers_QNAME, Getusers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Form }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "form")
    public JAXBElement<Form> createForm(Form value) {
        return new JAXBElement<Form>(_Form_QNAME, Form.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gettypecase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "gettypecase")
    public JAXBElement<Gettypecase> createGettypecase(Gettypecase value) {
        return new JAXBElement<Gettypecase>(_Gettypecase_QNAME, Gettypecase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetmainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getmainResponse")
    public JAXBElement<GetmainResponse> createGetmainResponse(GetmainResponse value) {
        return new JAXBElement<GetmainResponse>(_GetmainResponse_QNAME, GetmainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetusersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getusersResponse")
    public JAXBElement<GetusersResponse> createGetusersResponse(GetusersResponse value) {
        return new JAXBElement<GetusersResponse>(_GetusersResponse_QNAME, GetusersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Formpart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "formpart")
    public JAXBElement<Formpart> createFormpart(Formpart value) {
        return new JAXBElement<Formpart>(_Formpart_QNAME, Formpart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistfiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfiles")
    public JAXBElement<Getlistfiles> createGetlistfiles(Getlistfiles value) {
        return new JAXBElement<Getlistfiles>(_Getlistfiles_QNAME, Getlistfiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Skininfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "skininfo")
    public JAXBElement<Skininfo> createSkininfo(Skininfo value) {
        return new JAXBElement<Skininfo>(_Skininfo_QNAME, Skininfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getmaxid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getmaxid")
    public JAXBElement<Getmaxid> createGetmaxid(Getmaxid value) {
        return new JAXBElement<Getmaxid>(_Getmaxid_QNAME, Getmaxid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getdateResponse")
    public JAXBElement<GetdateResponse> createGetdateResponse(GetdateResponse value) {
        return new JAXBElement<GetdateResponse>(_GetdateResponse_QNAME, GetdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Todayspat }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "todayspat")
    public JAXBElement<Todayspat> createTodayspat(Todayspat value) {
        return new JAXBElement<Todayspat>(_Todayspat_QNAME, Todayspat.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistmain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistmain")
    public JAXBElement<Getlistmain> createGetlistmain(Getlistmain value) {
        return new JAXBElement<Getlistmain>(_Getlistmain_QNAME, Getlistmain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinduserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "finduserResponse")
    public JAXBElement<FinduserResponse> createFinduserResponse(FinduserResponse value) {
        return new JAXBElement<FinduserResponse>(_FinduserResponse_QNAME, FinduserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetuserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "setuserResponse")
    public JAXBElement<SetuserResponse> createSetuserResponse(SetuserResponse value) {
        return new JAXBElement<SetuserResponse>(_SetuserResponse_QNAME, SetuserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetnamemainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamemainResponse")
    public JAXBElement<GetnamemainResponse> createGetnamemainResponse(GetnamemainResponse value) {
        return new JAXBElement<GetnamemainResponse>(_GetnamemainResponse_QNAME, GetnamemainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Findnameforid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "findnameforid")
    public JAXBElement<Findnameforid> createFindnameforid(Findnameforid value) {
        return new JAXBElement<Findnameforid>(_Findnameforid_QNAME, Findnameforid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insertskin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertskin")
    public JAXBElement<Insertskin> createInsertskin(Insertskin value) {
        return new JAXBElement<Insertskin>(_Insertskin_QNAME, Insertskin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getnamepart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamepart")
    public JAXBElement<Getnamepart> createGetnamepart(Getnamepart value) {
        return new JAXBElement<Getnamepart>(_Getnamepart_QNAME, Getnamepart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistfilesbyidfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfilesbyidfile")
    public JAXBElement<Getlistfilesbyidfile> createGetlistfilesbyidfile(Getlistfilesbyidfile value) {
        return new JAXBElement<Getlistfilesbyidfile>(_Getlistfilesbyidfile_QNAME, Getlistfilesbyidfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Finduser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "finduser")
    public JAXBElement<Finduser> createFinduser(Finduser value) {
        return new JAXBElement<Finduser>(_Finduser_QNAME, Finduser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Fileinfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "fileinfo")
    public JAXBElement<Fileinfo> createFileinfo(Fileinfo value) {
        return new JAXBElement<Fileinfo>(_Fileinfo_QNAME, Fileinfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetnamepartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamepartResponse")
    public JAXBElement<GetnamepartResponse> createGetnamepartResponse(GetnamepartResponse value) {
        return new JAXBElement<GetnamepartResponse>(_GetnamepartResponse_QNAME, GetnamepartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertskinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertskinResponse")
    public JAXBElement<InsertskinResponse> createInsertskinResponse(InsertskinResponse value) {
        return new JAXBElement<InsertskinResponse>(_InsertskinResponse_QNAME, InsertskinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistinfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistinfo")
    public JAXBElement<Getlistinfo> createGetlistinfo(Getlistinfo value) {
        return new JAXBElement<Getlistinfo>(_Getlistinfo_QNAME, Getlistinfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetnamecaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamecaseResponse")
    public JAXBElement<GetnamecaseResponse> createGetnamecaseResponse(GetnamecaseResponse value) {
        return new JAXBElement<GetnamecaseResponse>(_GetnamecaseResponse_QNAME, GetnamecaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistcase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistcase")
    public JAXBElement<Getlistcase> createGetlistcase(Getlistcase value) {
        return new JAXBElement<Getlistcase>(_Getlistcase_QNAME, Getlistcase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getmain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getmain")
    public JAXBElement<Getmain> createGetmain(Getmain value) {
        return new JAXBElement<Getmain>(_Getmain_QNAME, Getmain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistcaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistcaseResponse")
    public JAXBElement<GetlistcaseResponse> createGetlistcaseResponse(GetlistcaseResponse value) {
        return new JAXBElement<GetlistcaseResponse>(_GetlistcaseResponse_QNAME, GetlistcaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Checkuser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "checkuser")
    public JAXBElement<Checkuser> createCheckuser(Checkuser value) {
        return new JAXBElement<Checkuser>(_Checkuser_QNAME, Checkuser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getallcase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getallcase")
    public JAXBElement<Getallcase> createGetallcase(Getallcase value) {
        return new JAXBElement<Getallcase>(_Getallcase_QNAME, Getallcase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Painrate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "painrate")
    public JAXBElement<Painrate> createPainrate(Painrate value) {
        return new JAXBElement<Painrate>(_Painrate_QNAME, Painrate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getdate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getdate")
    public JAXBElement<Getdate> createGetdate(Getdate value) {
        return new JAXBElement<Getdate>(_Getdate_QNAME, Getdate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistpartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistpartResponse")
    public JAXBElement<GetlistpartResponse> createGetlistpartResponse(GetlistpartResponse value) {
        return new JAXBElement<GetlistpartResponse>(_GetlistpartResponse_QNAME, GetlistpartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistmainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistmainResponse")
    public JAXBElement<GetlistmainResponse> createGetlistmainResponse(GetlistmainResponse value) {
        return new JAXBElement<GetlistmainResponse>(_GetlistmainResponse_QNAME, GetlistmainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertResponse")
    public JAXBElement<InsertResponse> createInsertResponse(InsertResponse value) {
        return new JAXBElement<InsertResponse>(_InsertResponse_QNAME, InsertResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistpatient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistpatient")
    public JAXBElement<Getlistpatient> createGetlistpatient(Getlistpatient value) {
        return new JAXBElement<Getlistpatient>(_Getlistpatient_QNAME, Getlistpatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistskin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistskin")
    public JAXBElement<Getlistskin> createGetlistskin(Getlistskin value) {
        return new JAXBElement<Getlistskin>(_Getlistskin_QNAME, Getlistskin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetmaxidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getmaxidResponse")
    public JAXBElement<GetmaxidResponse> createGetmaxidResponse(GetmaxidResponse value) {
        return new JAXBElement<GetmaxidResponse>(_GetmaxidResponse_QNAME, GetmaxidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertmfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertmfResponse")
    public JAXBElement<InsertmfResponse> createInsertmfResponse(InsertmfResponse value) {
        return new JAXBElement<InsertmfResponse>(_InsertmfResponse_QNAME, InsertmfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistfilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistfilesResponse")
    public JAXBElement<GetlistfilesResponse> createGetlistfilesResponse(GetlistfilesResponse value) {
        return new JAXBElement<GetlistfilesResponse>(_GetlistfilesResponse_QNAME, GetlistfilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Formcase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "formcase")
    public JAXBElement<Formcase> createFormcase(Formcase value) {
        return new JAXBElement<Formcase>(_Formcase_QNAME, Formcase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertfileResponse")
    public JAXBElement<InsertfileResponse> createInsertfileResponse(InsertfileResponse value) {
        return new JAXBElement<InsertfileResponse>(_InsertfileResponse_QNAME, InsertfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getallpart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getallpart")
    public JAXBElement<Getallpart> createGetallpart(Getallpart value) {
        return new JAXBElement<Getallpart>(_Getallpart_QNAME, Getallpart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetlistinfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistinfoResponse")
    public JAXBElement<GetlistinfoResponse> createGetlistinfoResponse(GetlistinfoResponse value) {
        return new JAXBElement<GetlistinfoResponse>(_GetlistinfoResponse_QNAME, GetlistinfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "updateResponse")
    public JAXBElement<UpdateResponse> createUpdateResponse(UpdateResponse value) {
        return new JAXBElement<UpdateResponse>(_UpdateResponse_QNAME, UpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GettypecaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "gettypecaseResponse")
    public JAXBElement<GettypecaseResponse> createGettypecaseResponse(GettypecaseResponse value) {
        return new JAXBElement<GettypecaseResponse>(_GettypecaseResponse_QNAME, GettypecaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getlistpart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getlistpart")
    public JAXBElement<Getlistpart> createGetlistpart(Getlistpart value) {
        return new JAXBElement<Getlistpart>(_Getlistpart_QNAME, Getlistpart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insertfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "insertfile")
    public JAXBElement<Insertfile> createInsertfile(Insertfile value) {
        return new JAXBElement<Insertfile>(_Insertfile_QNAME, Insertfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getnamecase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getnamecase")
    public JAXBElement<Getnamecase> createGetnamecase(Getnamecase value) {
        return new JAXBElement<Getnamecase>(_Getnamecase_QNAME, Getnamecase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetallpartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getallpartResponse")
    public JAXBElement<GetallpartResponse> createGetallpartResponse(GetallpartResponse value) {
        return new JAXBElement<GetallpartResponse>(_GetallpartResponse_QNAME, GetallpartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Medicalfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "medicalfile")
    public JAXBElement<Medicalfile> createMedicalfile(Medicalfile value) {
        return new JAXBElement<Medicalfile>(_Medicalfile_QNAME, Medicalfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetallcaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.dmi.csys.com/", name = "getallcaseResponse")
    public JAXBElement<GetallcaseResponse> createGetallcaseResponse(GetallcaseResponse value) {
        return new JAXBElement<GetallcaseResponse>(_GetallcaseResponse_QNAME, GetallcaseResponse.class, null, value);
    }

}
