
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour skininfo complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="skininfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="color" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://service.dmi.csys.com/}medicalfile" minOccurs="0"/>
 *         &lt;element name="place" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secretions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="skininfoPK" type="{http://service.dmi.csys.com/}skininfoPK" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "skininfo", propOrder = {
    "color",
    "medicalfile",
    "place",
    "secretions",
    "size",
    "skininfoPK"
})
public class Skininfo {

    protected String color;
    @XmlElement(namespace = "http://service.dmi.csys.com/")
    protected Medicalfile medicalfile;
    protected String place;
    protected String secretions;
    protected String size;
    protected SkininfoPK skininfoPK;

    /**
     * Obtient la valeur de la propriété color.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Définit la valeur de la propriété color.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Obtient la valeur de la propriété medicalfile.
     * 
     * @return
     *     possible object is
     *     {@link Medicalfile }
     *     
     */
    public Medicalfile getMedicalfile() {
        return medicalfile;
    }

    /**
     * Définit la valeur de la propriété medicalfile.
     * 
     * @param value
     *     allowed object is
     *     {@link Medicalfile }
     *     
     */
    public void setMedicalfile(Medicalfile value) {
        this.medicalfile = value;
    }

    /**
     * Obtient la valeur de la propriété place.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlace() {
        return place;
    }

    /**
     * Définit la valeur de la propriété place.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlace(String value) {
        this.place = value;
    }

    /**
     * Obtient la valeur de la propriété secretions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecretions() {
        return secretions;
    }

    /**
     * Définit la valeur de la propriété secretions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecretions(String value) {
        this.secretions = value;
    }

    /**
     * Obtient la valeur de la propriété size.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSize() {
        return size;
    }

    /**
     * Définit la valeur de la propriété size.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSize(String value) {
        this.size = value;
    }

    /**
     * Obtient la valeur de la propriété skininfoPK.
     * 
     * @return
     *     possible object is
     *     {@link SkininfoPK }
     *     
     */
    public SkininfoPK getSkininfoPK() {
        return skininfoPK;
    }

    /**
     * Définit la valeur de la propriété skininfoPK.
     * 
     * @param value
     *     allowed object is
     *     {@link SkininfoPK }
     *     
     */
    public void setSkininfoPK(SkininfoPK value) {
        this.skininfoPK = value;
    }

}
