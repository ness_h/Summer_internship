
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour fileinfo complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="fileinfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fileinfoPK" type="{http://service.dmi.csys.com/}fileinfoPK" minOccurs="0"/>
 *         &lt;element ref="{http://service.dmi.csys.com/}form" minOccurs="0"/>
 *         &lt;element ref="{http://service.dmi.csys.com/}formpart" minOccurs="0"/>
 *         &lt;element ref="{http://service.dmi.csys.com/}medicalfile" minOccurs="0"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fileinfo", propOrder = {
    "fileinfoPK",
    "form",
    "formpart",
    "medicalfile",
    "value"
})
public class Fileinfo {

    protected FileinfoPK fileinfoPK;
    @XmlElement(namespace = "http://service.dmi.csys.com/")
    protected Form form;
    @XmlElement(namespace = "http://service.dmi.csys.com/")
    protected Formpart formpart;
    @XmlElement(namespace = "http://service.dmi.csys.com/")
    protected Medicalfile medicalfile;
    protected String value;

    /**
     * Obtient la valeur de la propriété fileinfoPK.
     * 
     * @return
     *     possible object is
     *     {@link FileinfoPK }
     *     
     */
    public FileinfoPK getFileinfoPK() {
        return fileinfoPK;
    }

    /**
     * Définit la valeur de la propriété fileinfoPK.
     * 
     * @param value
     *     allowed object is
     *     {@link FileinfoPK }
     *     
     */
    public void setFileinfoPK(FileinfoPK value) {
        this.fileinfoPK = value;
    }

    /**
     * Obtient la valeur de la propriété form.
     * 
     * @return
     *     possible object is
     *     {@link Form }
     *     
     */
    public Form getForm() {
        return form;
    }

    /**
     * Définit la valeur de la propriété form.
     * 
     * @param value
     *     allowed object is
     *     {@link Form }
     *     
     */
    public void setForm(Form value) {
        this.form = value;
    }

    /**
     * Obtient la valeur de la propriété formpart.
     * 
     * @return
     *     possible object is
     *     {@link Formpart }
     *     
     */
    public Formpart getFormpart() {
        return formpart;
    }

    /**
     * Définit la valeur de la propriété formpart.
     * 
     * @param value
     *     allowed object is
     *     {@link Formpart }
     *     
     */
    public void setFormpart(Formpart value) {
        this.formpart = value;
    }

    /**
     * Obtient la valeur de la propriété medicalfile.
     * 
     * @return
     *     possible object is
     *     {@link Medicalfile }
     *     
     */
    public Medicalfile getMedicalfile() {
        return medicalfile;
    }

    /**
     * Définit la valeur de la propriété medicalfile.
     * 
     * @param value
     *     allowed object is
     *     {@link Medicalfile }
     *     
     */
    public void setMedicalfile(Medicalfile value) {
        this.medicalfile = value;
    }

    /**
     * Obtient la valeur de la propriété value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Définit la valeur de la propriété value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
