
package service;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6b21 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "PatientWS", targetNamespace = "http://service.dmi.csys.com/", wsdlLocation = "file:/C:/Users/G50/Documents/NetBeansProjects/Projet/dmi-core/target/generated-sources/wsdl/PatientWS.wsdl")
public class PatientWS_Service
    extends Service
{

    private final static URL PATIENTWS_WSDL_LOCATION;
    private final static WebServiceException PATIENTWS_EXCEPTION;
    private final static QName PATIENTWS_QNAME = new QName("http://service.dmi.csys.com/", "PatientWS");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/G50/Documents/NetBeansProjects/Projet/dmi-core/target/generated-sources/wsdl/PatientWS.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        PATIENTWS_WSDL_LOCATION = url;
        PATIENTWS_EXCEPTION = e;
    }

    public PatientWS_Service() {
        super(__getWsdlLocation(), PATIENTWS_QNAME);
    }

    public PatientWS_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), PATIENTWS_QNAME, features);
    }

    public PatientWS_Service(URL wsdlLocation) {
        super(wsdlLocation, PATIENTWS_QNAME);
    }

    public PatientWS_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, PATIENTWS_QNAME, features);
    }

    public PatientWS_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PatientWS_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PatientWS
     */
    @WebEndpoint(name = "PatientWSPort")
    public PatientWS getPatientWSPort() {
        return super.getPort(new QName("http://service.dmi.csys.com/", "PatientWSPort"), PatientWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PatientWS
     */
    @WebEndpoint(name = "PatientWSPort")
    public PatientWS getPatientWSPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://service.dmi.csys.com/", "PatientWSPort"), PatientWS.class, features);
    }

    private static URL __getWsdlLocation() {
        if (PATIENTWS_EXCEPTION!= null) {
            throw PATIENTWS_EXCEPTION;
        }
        return PATIENTWS_WSDL_LOCATION;
    }

}
