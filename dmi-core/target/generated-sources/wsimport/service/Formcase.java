
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour formcase complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="formcase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idcase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idpart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nameArb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formcase", propOrder = {
    "idcase",
    "idpart",
    "name",
    "nameArb",
    "type"
})
public class Formcase {

    protected String idcase;
    protected String idpart;
    protected String name;
    protected String nameArb;
    protected String type;

    /**
     * Obtient la valeur de la propriété idcase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdcase() {
        return idcase;
    }

    /**
     * Définit la valeur de la propriété idcase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdcase(String value) {
        this.idcase = value;
    }

    /**
     * Obtient la valeur de la propriété idpart.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdpart() {
        return idpart;
    }

    /**
     * Définit la valeur de la propriété idpart.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdpart(String value) {
        this.idpart = value;
    }

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété nameArb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameArb() {
        return nameArb;
    }

    /**
     * Définit la valeur de la propriété nameArb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameArb(String value) {
        this.nameArb = value;
    }

    /**
     * Obtient la valeur de la propriété type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Définit la valeur de la propriété type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
