
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Medicalfile;

@XmlRootElement(name = "getlistfilesResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistfilesResponse", namespace = "http://service.dmi.csys.com/")
public class GetlistfilesResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Medicalfile> _return;

    /**
     * 
     * @return
     *     returns List<Medicalfile>
     */
    public List<Medicalfile> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Medicalfile> _return) {
        this._return = _return;
    }

}
