
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Log;

@XmlRootElement(name = "getusersResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getusersResponse", namespace = "http://service.dmi.csys.com/")
public class GetusersResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Log> _return;

    /**
     * 
     * @return
     *     returns List<Log>
     */
    public List<Log> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Log> _return) {
        this._return = _return;
    }

}
