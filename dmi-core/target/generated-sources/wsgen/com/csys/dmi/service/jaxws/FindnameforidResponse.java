
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Patient;

@XmlRootElement(name = "findnameforidResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findnameforidResponse", namespace = "http://service.dmi.csys.com/")
public class FindnameforidResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Patient> _return;

    /**
     * 
     * @return
     *     returns List<Patient>
     */
    public List<Patient> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Patient> _return) {
        this._return = _return;
    }

}
