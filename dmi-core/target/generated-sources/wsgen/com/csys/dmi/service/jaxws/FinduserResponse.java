
package com.csys.dmi.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Log;

@XmlRootElement(name = "finduserResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "finduserResponse", namespace = "http://service.dmi.csys.com/")
public class FinduserResponse {

    @XmlElement(name = "return", namespace = "")
    private Log _return;

    /**
     * 
     * @return
     *     returns Log
     */
    public Log getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(Log _return) {
        this._return = _return;
    }

}
