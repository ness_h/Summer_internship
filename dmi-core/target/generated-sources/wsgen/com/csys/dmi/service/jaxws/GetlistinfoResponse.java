
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Fileinfo;

@XmlRootElement(name = "getlistinfoResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistinfoResponse", namespace = "http://service.dmi.csys.com/")
public class GetlistinfoResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Fileinfo> _return;

    /**
     * 
     * @return
     *     returns List<Fileinfo>
     */
    public List<Fileinfo> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Fileinfo> _return) {
        this._return = _return;
    }

}
