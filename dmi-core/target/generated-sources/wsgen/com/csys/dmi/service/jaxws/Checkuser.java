
package com.csys.dmi.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Log;

@XmlRootElement(name = "checkuser", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkuser", namespace = "http://service.dmi.csys.com/", propOrder = {
    "arg0",
    "arg1"
})
public class Checkuser {

    @XmlElement(name = "arg0", namespace = "")
    private Log arg0;
    @XmlElement(name = "arg1", namespace = "")
    private Log arg1;

    /**
     * 
     * @return
     *     returns Log
     */
    public Log getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(Log arg0) {
        this.arg0 = arg0;
    }

    /**
     * 
     * @return
     *     returns Log
     */
    public Log getArg1() {
        return this.arg1;
    }

    /**
     * 
     * @param arg1
     *     the value for the arg1 property
     */
    public void setArg1(Log arg1) {
        this.arg1 = arg1;
    }

}
