
package com.csys.dmi.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getlistfiles", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistfiles", namespace = "http://service.dmi.csys.com/")
public class Getlistfiles {


}
