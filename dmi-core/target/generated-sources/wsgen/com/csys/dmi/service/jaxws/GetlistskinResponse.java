
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Skininfo;

@XmlRootElement(name = "getlistskinResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistskinResponse", namespace = "http://service.dmi.csys.com/")
public class GetlistskinResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Skininfo> _return;

    /**
     * 
     * @return
     *     returns List<Skininfo>
     */
    public List<Skininfo> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Skininfo> _return) {
        this._return = _return;
    }

}
