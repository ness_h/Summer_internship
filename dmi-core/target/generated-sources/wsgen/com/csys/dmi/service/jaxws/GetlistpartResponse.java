
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Formpart;

@XmlRootElement(name = "getlistpartResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistpartResponse", namespace = "http://service.dmi.csys.com/")
public class GetlistpartResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Formpart> _return;

    /**
     * 
     * @return
     *     returns List<Formpart>
     */
    public List<Formpart> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Formpart> _return) {
        this._return = _return;
    }

}
