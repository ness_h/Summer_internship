
package com.csys.dmi.service.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.csys.dmi.model.Formcase;

@XmlRootElement(name = "getlistcaseResponse", namespace = "http://service.dmi.csys.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getlistcaseResponse", namespace = "http://service.dmi.csys.com/")
public class GetlistcaseResponse {

    @XmlElement(name = "return", namespace = "")
    private List<Formcase> _return;

    /**
     * 
     * @return
     *     returns List<Formcase>
     */
    public List<Formcase> getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(List<Formcase> _return) {
        this._return = _return;
    }

}
