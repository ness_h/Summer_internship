package com.csys.dmi.model;

import com.csys.dmi.model.Medicalfile;
import com.csys.dmi.model.SkininfoPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:40")
@StaticMetamodel(Skininfo.class)
public class Skininfo_ { 

    public static volatile SingularAttribute<Skininfo, String> size;
    public static volatile SingularAttribute<Skininfo, String> color;
    public static volatile SingularAttribute<Skininfo, SkininfoPK> skininfoPK;
    public static volatile SingularAttribute<Skininfo, String> place;
    public static volatile SingularAttribute<Skininfo, Medicalfile> medicalfile;
    public static volatile SingularAttribute<Skininfo, String> secretions;

}