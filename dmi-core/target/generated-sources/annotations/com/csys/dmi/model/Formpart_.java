package com.csys.dmi.model;

import com.csys.dmi.model.Fileinfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:41")
@StaticMetamodel(Formpart.class)
public class Formpart_ { 

    public static volatile SingularAttribute<Formpart, String> nameArb;
    public static volatile SingularAttribute<Formpart, String> idmain;
    public static volatile SingularAttribute<Formpart, String> name;
    public static volatile SingularAttribute<Formpart, String> idpart;
    public static volatile CollectionAttribute<Formpart, Fileinfo> fileinfoCollection;

}