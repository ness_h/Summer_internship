package com.csys.dmi.model;

import com.csys.dmi.model.Fileinfo;
import com.csys.dmi.model.Skininfo;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:41")
@StaticMetamodel(Medicalfile.class)
public class Medicalfile_ { 

    public static volatile SingularAttribute<Medicalfile, String> date;
    public static volatile SingularAttribute<Medicalfile, String> idpatient;
    public static volatile SingularAttribute<Medicalfile, Integer> painrate;
    public static volatile SingularAttribute<Medicalfile, Integer> respiratoryrate;
    public static volatile CollectionAttribute<Medicalfile, Skininfo> skininfoCollection;
    public static volatile SingularAttribute<Medicalfile, Integer> pulse;
    public static volatile SingularAttribute<Medicalfile, Integer> bodytemperature;
    public static volatile SingularAttribute<Medicalfile, Integer> weight;
    public static volatile SingularAttribute<Medicalfile, String> idfile;
    public static volatile SingularAttribute<Medicalfile, String> bloodpressure;
    public static volatile SingularAttribute<Medicalfile, String> diagnostic;
    public static volatile CollectionAttribute<Medicalfile, Fileinfo> fileinfoCollection;

}