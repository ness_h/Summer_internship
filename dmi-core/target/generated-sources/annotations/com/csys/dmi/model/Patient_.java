package com.csys.dmi.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:40")
@StaticMetamodel(Patient.class)
public class Patient_ { 

    public static volatile SingularAttribute<Patient, String> idpatient;
    public static volatile SingularAttribute<Patient, String> birthdate;
    public static volatile SingularAttribute<Patient, String> name;
    public static volatile SingularAttribute<Patient, String> phonenumber;
    public static volatile SingularAttribute<Patient, String> cin;
    public static volatile SingularAttribute<Patient, String> adress;
    public static volatile SingularAttribute<Patient, String> sexe;

}