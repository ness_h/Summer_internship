package com.csys.dmi.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:40")
@StaticMetamodel(FileinfoPK.class)
public class FileinfoPK_ { 

    public static volatile SingularAttribute<FileinfoPK, String> idmain;
    public static volatile SingularAttribute<FileinfoPK, String> idcase;
    public static volatile SingularAttribute<FileinfoPK, String> idfile;
    public static volatile SingularAttribute<FileinfoPK, String> idpart;

}