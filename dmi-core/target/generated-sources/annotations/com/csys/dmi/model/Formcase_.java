package com.csys.dmi.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:40")
@StaticMetamodel(Formcase.class)
public class Formcase_ { 

    public static volatile SingularAttribute<Formcase, String> nameArb;
    public static volatile SingularAttribute<Formcase, String> idcase;
    public static volatile SingularAttribute<Formcase, String> name;
    public static volatile SingularAttribute<Formcase, String> idpart;
    public static volatile SingularAttribute<Formcase, String> type;

}