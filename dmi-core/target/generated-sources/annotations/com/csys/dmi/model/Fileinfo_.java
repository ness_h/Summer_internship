package com.csys.dmi.model;

import com.csys.dmi.model.FileinfoPK;
import com.csys.dmi.model.Form;
import com.csys.dmi.model.Formpart;
import com.csys.dmi.model.Medicalfile;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150916-rNA", date="2016-07-29T16:18:41")
@StaticMetamodel(Fileinfo.class)
public class Fileinfo_ { 

    public static volatile SingularAttribute<Fileinfo, Form> form;
    public static volatile SingularAttribute<Fileinfo, FileinfoPK> fileinfoPK;
    public static volatile SingularAttribute<Fileinfo, Medicalfile> medicalfile;
    public static volatile SingularAttribute<Fileinfo, String> value;
    public static volatile SingularAttribute<Fileinfo, Formpart> formpart;

}