/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.service;

import com.csys.dmi.dao.LogDAO;
import com.csys.dmi.dao.MedicalfileDAO;
import com.csys.dmi.dao.PatientDAO;
import com.csys.dmi.dao.formDAO;
import com.csys.dmi.model.Fileinfo;
import com.csys.dmi.model.Form;
import com.csys.dmi.model.Formcase;
import com.csys.dmi.model.Formpart;
import com.csys.dmi.model.Log;
import com.csys.dmi.model.Medicalfile;
import com.csys.dmi.model.Patient;
import com.csys.dmi.model.Skininfo;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author G50
 */
@WebService(serviceName = "PatientWS")
public class PatientWS {

    /**(@WebParam(name = "name") String txt)
     * This is a sample web service operation
     * @return 
     */
    @WebMethod(operationName = "getlistpatient")
    public List<Patient> getlistpatient() {
        return new PatientDAO().getlistpatient();
    }
    @WebMethod(operationName = "update")
    public void update(String idpatient,String name,Date birthdate,String adress,String sexe,String phonenumber,String CIN){
       new PatientDAO().update(idpatient, name, birthdate, adress,sexe,phonenumber,CIN);
    }
     @WebMethod(operationName = "insert")
    public void insert(String idpatient,String name,Date birthdate,String adress,String sexe,String phonenumber,String CIN){
       new PatientDAO().insert(idpatient, name, birthdate,adress,sexe,phonenumber,CIN);
    }
      @WebMethod(operationName = "findnameforid")
    public List<Patient> findnameforid(String idpatient){
        return new PatientDAO().findnameforid(idpatient);
       
    }
    
    @WebMethod(operationName = "getlistfiles")
    public List<Medicalfile> getlistfiles(){
        return new MedicalfileDAO().getlistfiles();
    }
    @WebMethod(operationName = "insertmf")
    public void insertmf(String idpatient,String idfile,Date date,Integer pulse,Integer respiratoryrate,String bloodpressure,Integer bodytemperature,Integer painrate,Integer weight,String diagnostic){
    new MedicalfileDAO().insertmf(idpatient, idfile, date, pulse, respiratoryrate, bloodpressure, bodytemperature, painrate, weight, diagnostic);
    }
    @WebMethod(operationName = "getdate")
    public String getdate(String idfile) {
        return new MedicalfileDAO().getdate(idfile);
    }
    
      @WebMethod(operationName = "getnamemain")
    public String getnamemain(String idmain){
        return new formDAO().getnamemain(idmain);
    }
      @WebMethod(operationName = "getnamepart")
    public String getnamepart(String idpart){
        return new formDAO().getnamepart(idpart);
    }
    @WebMethod(operationName = "getnamecase")
    public String getnamecase(String idcase){
        return new formDAO().getnamecase(idcase);
    }
    @WebMethod(operationName = "painrate")
    public long painrate(Integer painrate){
        return new MedicalfileDAO().painrate(painrate);
    }
    @WebMethod(operationName ="todayspat")
    public long todayspat(Date d){
        return new MedicalfileDAO().todayspat(d);
    }
    @WebMethod(operationName = "getusers")
    public List<Log> getusers(){
        return new LogDAO().getusers();
    }
        @WebMethod(operationName = "finduser")
    public Log finduser(String username){
        return new LogDAO().finduser(username);
    }
    @WebMethod(operationName = "setuser")
    public Log setuser(String username,String password){
        return new LogDAO().setuser(username, password);
    }
    @WebMethod(operationName = "checkuser")
    public boolean checkuser(Log user, Log user1){
        return new LogDAO().checkuser(user, user1);
    }
    @WebMethod(operationName = "getlistpart")
     public List<Formpart> getlistpart(String idmain){
        return new formDAO().getlistpart(idmain);
    }
    @WebMethod(operationName = "getlistcase")
    public List<Formcase> getlistcase(String idpart){
        return new formDAO().getlistcase(idpart);
    }
    @WebMethod(operationName = "getlistinfo")
    public List<Fileinfo> getlistinfo(String idfile){
        return new formDAO().getlistinfo(idfile);
    }

    /**
     *
     * @param idfile
     * @param idmain
     * @param idpart
     * @param idcase
     * @param value
     */
    @WebMethod(operationName = "insertfile")
    public void insertfile(String idfile, String idmain, String idpart, String idcase,String value){
         new formDAO().insertfile(idfile, idmain, idpart, idcase, value);
    }
        @WebMethod(operationName = "getlistmain")
    public List<Form> getlistmain(){
        return new formDAO().getlistmain();
    }
            @WebMethod(operationName = "gettypecase")
        public String gettypecase(String idcase){
            return new formDAO().gettypecase(idcase);
        }
         @WebMethod(operationName = "getmain")
        public String getmain(String idpart){
     return new formDAO().getmain(idpart);
        }
        @WebMethod(operationName = "getallcase")
        public List<Formcase> getallcase(){
            return new formDAO().getallcase();
        }
        @WebMethod(operationName = "getallpart")
        public List<Formpart> getallpart(){
            return new formDAO().getallpart();
        }
        @WebMethod(operationName = "getlistfilesbyidpatient")
        public List<Medicalfile> getlistfilesbyidpatient(String idpatient){
        return new MedicalfileDAO().getlistfilesbyidpatient(idpatient);
        }
         @WebMethod(operationName = "getlistfilesbyidfile")
        public List<Medicalfile> getlistfilesbyidfile(String idfile){
            return new MedicalfileDAO().getlistfilesbyidfile(idfile);
        }
        @WebMethod(operationName = "getlistskin")
        public List<Skininfo> getlistskin(String idfile){
           return new MedicalfileDAO().getlistskin(idfile);
        } 
        @WebMethod(operationName = "insertskin")
        public void insertskin(String idfile,String idskin,String place,String size,String color,String secretions){
         new MedicalfileDAO().insertskin(idfile, idskin, place, size, color, secretions);
        }
        @WebMethod(operationName = "getmaxid")
        public String getmaxid(){
            return new MedicalfileDAO().getmaxid();
        }


}
