/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.dao;



import com.csys.dmi.model.Log;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author G50
 */
public class LogDAO {
    
    EntityManager em=FactoriesRepository.GetEntityManager(FactoriesRepository.getDmiPU());
    
     public List<Log> getusers(){
        
        Query query =em.createNamedQuery("Log.findAll", Log.class);
        List <Log> list= query.getResultList();
        return list;
    }
     
     public Log finduser(String username){
         Log user = (Log) em.createNamedQuery("Log.findByUsername", Log.class).setParameter("username",username).getSingleResult();
        return user;
     }
     
     public Log setuser(String username, String password){
         Log user = new Log(username,password);
         return user;
     }
     public boolean checkuser(Log user, Log user1){
        return user1.getUsername().equals(user.getUsername())&&(user.getPassword() == null ? user1.getPassword() == null : user.getPassword().equals(user1.getPassword()));
     }
}
