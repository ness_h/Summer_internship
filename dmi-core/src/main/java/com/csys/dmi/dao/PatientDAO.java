/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.dao;

import com.csys.dmi.model.Patient;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author G50
 */
public class PatientDAO {

    EntityManager em = FactoriesRepository.GetEntityManager(FactoriesRepository.getDmiPU());
    EntityTransaction tx = em.getTransaction();

    public List<Patient> getlistpatient() {
        Query query = em.createNamedQuery("Patient.findAll", Patient.class);
        return query.getResultList();
    }

    public void insert(String idpatient, String name, Date birthdate, String adress, String sexe, String phonenumber, String CIN) {
        tx.begin();
        Query query = em.createNativeQuery("INSERT INTO Patient (idpatient,name,birthdate,adress,sexe,phonenumber,CIN) "
                + " VALUES(?,?,?,?,?,?,?)");
        query.setParameter(1, idpatient);
        query.setParameter(2, name);
        query.setParameter(3, birthdate);
        query.setParameter(4, adress);
        query.setParameter(5, sexe);
        query.setParameter(6, phonenumber);
        query.setParameter(7, CIN);
        query.executeUpdate();
        tx.commit();
    }

    public void update(String idpatient, String name, Date birthdate, String adress, String sexe, String phonenumber, String CIN) {
        tx.begin();
        Query query = em.createNativeQuery("UPDATE patient SET name = ?, birthdate = ?, adress = ?,sexe = ?,phonenumber = ?,CIN = ? WHERE idpatient = ?");
        query.setParameter(1, name);
        query.setParameter(2, birthdate);
        query.setParameter(3, adress);
        query.setParameter(4, sexe);
        query.setParameter(5, phonenumber);
        query.setParameter(6, CIN);
        query.setParameter(7, idpatient);
        query.executeUpdate();
        tx.commit();
    }

    public List<Patient> findnameforid(String idpatient) {

        Query query = em.createNamedQuery("Patient.findByIdpatient", Patient.class).setParameter("idpatient",
                idpatient);
        return query.getResultList();

    }

  
}
