
package com.csys.dmi.dao;


import com.csys.dmi.model.Medicalfile;
import com.csys.dmi.model.Skininfo;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author G50
 */
public class MedicalfileDAO {
    
     EntityManager em=FactoriesRepository.GetEntityManager(FactoriesRepository.getDmiPU());
    EntityTransaction tx=em.getTransaction();
    
     public List<Medicalfile> getlistfiles(){
        Query query =em.createNamedQuery("Medicalfile.findAll", Medicalfile.class);
        List <Medicalfile> list=query.getResultList();
        return list;
    }
     
      public void insertmf(String idpatient,String idfile,Date date,Integer pulse,Integer respiratoryrate,String bloodpressure,Integer bodytemperature,Integer painrate,Integer weight,String diagnostic){
        tx.begin();
          Query query = em.createNativeQuery("INSERT INTO Medicalfile (idpatient,idfile,date,pulse,bloodpressure,respiratoryrate,bodytemperature,painrate,weight,diagnostic) " +
            " VALUES(?,?,?,?,?,?,?,?,?,?)");
        query.setParameter(1, idpatient);
        query.setParameter(2, idfile);
        query.setParameter(3, date);
        query.setParameter(4, pulse);
        query.setParameter(5, bloodpressure);
        query.setParameter(6,respiratoryrate);
        query.setParameter(7,bodytemperature);
        query.setParameter(8,painrate);
        query.setParameter(9,weight);
        query.setParameter(10,diagnostic);
       
        query.executeUpdate();
        tx.commit();
    } 
      public long painrate(Integer painrate)
      {
          long num = ((Number)em.createNativeQuery("SELECT COUNT(*) FROM Medicalfile Where painrate=?").setParameter(1, painrate).getSingleResult()).longValue();
          return  num;
      }
      public long todayspat(Date d)
      {
          long num = ((Number)em.createNativeQuery("SELECT COUNT(*) FROM Medicalfile Where date=?").setParameter(1, d).getSingleResult()).longValue();
          return  num;
      }
      
      public String getdate(String idfile) {
    try {
      Medicalfile p= (Medicalfile) em.createNamedQuery("Medicalfile.findByIdfile").setParameter("idfile",
          idfile).getSingleResult();
      return p.getDate();
    } catch (NoResultException e) {
      return "";
    }
  }
     public List<Medicalfile> getlistfilesbyidpatient(String idpatient){
       Query query=em.createNamedQuery("Medicalfile.findByIdpatient",Medicalfile.class).setParameter("idpatient",idpatient);
       
       return query.getResultList();
       
    }
     public List<Medicalfile> getlistfilesbyidfile(String idfile){
        Query query =em.createNamedQuery("Medicalfile.findByIdfile",Medicalfile.class).setParameter("idfile",idfile);
        return query.getResultList();
    }
     public List<Skininfo> getlistskin(String idfile){
        Query query =em.createNamedQuery("Skininfo.findByIdfile",Skininfo.class).setParameter("idfile",idfile);
        return query.getResultList();
    }
     public void insertskin(String idfile,String idskin,String place,String size,String color,String secretions){
        tx.begin();
          Query query = em.createNativeQuery("INSERT INTO Skininfo (idfile,idskin,place,size,color,secretions) " +
            " VALUES(?,?,?,?,?,?)");
        query.setParameter(1, idfile);
        query.setParameter(2, idskin);
        query.setParameter(3, place);
        query.setParameter(4, size);
        query.setParameter(5, color);
        query.setParameter(6,secretions);
        
        query.executeUpdate();
        tx.commit();
    }
     public String getmaxid(){
        
         Query query=em.createNativeQuery("select max(idfile) from Medicalfile");
         return (String) query.getSingleResult();
     }
     
}
