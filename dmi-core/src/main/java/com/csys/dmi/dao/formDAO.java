/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.dao;

import com.csys.dmi.model.Fileinfo;
import com.csys.dmi.model.Form;
import com.csys.dmi.model.Formcase;
import com.csys.dmi.model.Formpart;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author G50
 */
public class formDAO {
    
    EntityManager em=FactoriesRepository.GetEntityManager(FactoriesRepository.getDmiPU());
    
    EntityTransaction tx=em.getTransaction();
    
    public String getnamemain(String idmain){
        Form query =(Form) em.createNamedQuery("Form.findByIdmain").setParameter("idmain",
          idmain).getSingleResult();
        return query.getName();
    }
    public String getnamepart(String idpart){
        Formpart query =(Formpart) em.createNamedQuery("Formpart.findByIdpart").setParameter("idpart",
          idpart).getSingleResult();
        return query.getName();
    }
    public String getnamecase(String idcase){
        Formcase query =(Formcase) em.createNamedQuery("Formcase.findByIdcase").setParameter("idcase",
          idcase).getSingleResult();
        return query.getName();
    }
    
      public List<Form> getlistmain(){
        Query query =em.createNamedQuery("Form.findAll", Form.class);
        return query.getResultList();
    }
    public List<Formpart> getlistpart(String idmain){
        Query query =em.createNamedQuery("Formpart.findByIdmain", Formpart.class).setParameter("idmain",idmain);
        return query.getResultList();
    }
    public List<Formcase> getlistcase(String idpart){
        Query query =em.createNamedQuery("Formcase.findByIdpart", Formcase.class).setParameter("idpart",idpart);
        return query.getResultList();
    }
    public String gettypecase(String idcase){
        Formcase query =(Formcase) em.createNamedQuery("Formcase.findByIdcase").setParameter("idcase",
          idcase).getSingleResult();
        return query.getType();
    }
            
    public void insertfile(String idfile, String idmain, String idpart, String idcase,String value){
        tx.begin();
        Query query = em.createNativeQuery("INSERT INTO Fileinfo (idfile,idmain,idpart,idcase,value) " +
            " VALUES(?,?,?,?,?)");
        query.setParameter(1, idfile);
        query.setParameter(2, idmain);
        query.setParameter(3, idpart);
        query.setParameter(4, idcase);
         query.setParameter(5, value);
        query.executeUpdate();
        tx.commit();
    }
    public List<Fileinfo> getlistinfo(String idfile){
        Query query =em.createNamedQuery("Fileinfo.findByIdfile", Fileinfo.class).setParameter("idfile", idfile);
        return query.getResultList();
    }
    
     public List<Formcase> getallcase(){
         Query query=em.createNamedQuery("Formcase.findAll",Formcase.class);
         return query.getResultList();
     }
     
     public List<Formpart> getallpart(){
         Query query=em.createNamedQuery("Formpart.findAll",Formpart.class);
         return query.getResultList();
     }
   
    public String getmain(String idpart)
    {
         Formpart p= (Formpart) em.createNamedQuery("Formpart.findByIdpart").setParameter("idpart",
          idpart).getSingleResult();
      return p.getIdmain();

    }
   
}
