/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Medicalfile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medicalfile.findAll", query = "SELECT m FROM Medicalfile m"),
    @NamedQuery(name = "Medicalfile.findByIdpatient", query = "SELECT m FROM Medicalfile m WHERE m.idpatient = :idpatient"),
    @NamedQuery(name = "Medicalfile.findByIdfile", query = "SELECT m FROM Medicalfile m WHERE m.idfile = :idfile"),
    @NamedQuery(name = "Medicalfile.findByDate", query = "SELECT m FROM Medicalfile m WHERE m.date = :date"),
    @NamedQuery(name = "Medicalfile.findByPulse", query = "SELECT m FROM Medicalfile m WHERE m.pulse = :pulse"),
    @NamedQuery(name = "Medicalfile.findByBloodpressure", query = "SELECT m FROM Medicalfile m WHERE m.bloodpressure = :bloodpressure"),
    @NamedQuery(name = "Medicalfile.findByRespiratoryrate", query = "SELECT m FROM Medicalfile m WHERE m.respiratoryrate = :respiratoryrate"),
    @NamedQuery(name = "Medicalfile.findByBodytemperature", query = "SELECT m FROM Medicalfile m WHERE m.bodytemperature = :bodytemperature"),
    @NamedQuery(name = "Medicalfile.findByPainrate", query = "SELECT m FROM Medicalfile m WHERE m.painrate = :painrate"),
    @NamedQuery(name = "Medicalfile.findByWeight", query = "SELECT m FROM Medicalfile m WHERE m.weight = :weight")})
public class Medicalfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idpatient")
    private String idpatient;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idfile")
    private String idfile;
    @Size(max = 10)
    @Column(name = "date")
    private String date;
    @Column(name = "pulse")
    private Integer pulse;
    @Size(max = 10)
    @Column(name = "bloodpressure")
    private String bloodpressure;
    @Column(name = "respiratoryrate")
    private Integer respiratoryrate;
    @Column(name = "bodytemperature")
    private Integer bodytemperature;
    @Column(name = "painrate")
    private Integer painrate;
    @Column(name = "weight")
    private Integer weight;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "diagnostic")
    private String diagnostic;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalfile")
    private Collection<Skininfo> skininfoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalfile")
    private Collection<Fileinfo> fileinfoCollection;

    public Medicalfile() {
    }

    public Medicalfile(String idfile) {
        this.idfile = idfile;
    }

    public Medicalfile(String idfile, String idpatient) {
        this.idfile = idfile;
        this.idpatient = idpatient;
    }

    public String getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(String idpatient) {
        this.idpatient = idpatient;
    }

    public String getIdfile() {
        return idfile;
    }

    public void setIdfile(String idfile) {
        this.idfile = idfile;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPulse() {
        return pulse;
    }

    public void setPulse(Integer pulse) {
        this.pulse = pulse;
    }

    public String getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(String bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public Integer getRespiratoryrate() {
        return respiratoryrate;
    }

    public void setRespiratoryrate(Integer respiratoryrate) {
        this.respiratoryrate = respiratoryrate;
    }

    public Integer getBodytemperature() {
        return bodytemperature;
    }

    public void setBodytemperature(Integer bodytemperature) {
        this.bodytemperature = bodytemperature;
    }

    public Integer getPainrate() {
        return painrate;
    }

    public void setPainrate(Integer painrate) {
        this.painrate = painrate;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    @XmlTransient
    public Collection<Skininfo> getSkininfoCollection() {
        return skininfoCollection;
    }

    public void setSkininfoCollection(Collection<Skininfo> skininfoCollection) {
        this.skininfoCollection = skininfoCollection;
    }

    @XmlTransient
    public Collection<Fileinfo> getFileinfoCollection() {
        return fileinfoCollection;
    }

    public void setFileinfoCollection(Collection<Fileinfo> fileinfoCollection) {
        this.fileinfoCollection = fileinfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfile != null ? idfile.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medicalfile)) {
            return false;
        }
        Medicalfile other = (Medicalfile) object;
        if ((this.idfile == null && other.idfile != null) || (this.idfile != null && !this.idfile.equals(other.idfile))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Medicalfile[ idfile=" + idfile + " ]";
    }
    
}
