/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Skininfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Skininfo.findAll", query = "SELECT s FROM Skininfo s"),
    @NamedQuery(name = "Skininfo.findByIdfile", query = "SELECT s FROM Skininfo s WHERE s.skininfoPK.idfile = :idfile"),
    @NamedQuery(name = "Skininfo.findByIdskin", query = "SELECT s FROM Skininfo s WHERE s.skininfoPK.idskin = :idskin"),
    @NamedQuery(name = "Skininfo.findByPlace", query = "SELECT s FROM Skininfo s WHERE s.place = :place"),
    @NamedQuery(name = "Skininfo.findBySize", query = "SELECT s FROM Skininfo s WHERE s.size = :size"),
    @NamedQuery(name = "Skininfo.findByColor", query = "SELECT s FROM Skininfo s WHERE s.color = :color"),
    @NamedQuery(name = "Skininfo.findBySecretions", query = "SELECT s FROM Skininfo s WHERE s.secretions = :secretions")})
public class Skininfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SkininfoPK skininfoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "place")
    private String place;
    @Size(max = 50)
    @Column(name = "size")
    private String size;
    @Size(max = 50)
    @Column(name = "color")
    private String color;
    @Size(max = 50)
    @Column(name = "secretions")
    private String secretions;
    @JoinColumn(name = "idfile", referencedColumnName = "idfile", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Medicalfile medicalfile;

    public Skininfo() {
    }

    public Skininfo(SkininfoPK skininfoPK) {
        this.skininfoPK = skininfoPK;
    }

    public Skininfo(SkininfoPK skininfoPK, String place) {
        this.skininfoPK = skininfoPK;
        this.place = place;
    }

    public Skininfo(String idfile, String idskin) {
        this.skininfoPK = new SkininfoPK(idfile, idskin);
    }

    public SkininfoPK getSkininfoPK() {
        return skininfoPK;
    }

    public void setSkininfoPK(SkininfoPK skininfoPK) {
        this.skininfoPK = skininfoPK;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSecretions() {
        return secretions;
    }

    public void setSecretions(String secretions) {
        this.secretions = secretions;
    }

    public Medicalfile getMedicalfile() {
        return medicalfile;
    }

    public void setMedicalfile(Medicalfile medicalfile) {
        this.medicalfile = medicalfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (skininfoPK != null ? skininfoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Skininfo)) {
            return false;
        }
        Skininfo other = (Skininfo) object;
        if ((this.skininfoPK == null && other.skininfoPK != null) || (this.skininfoPK != null && !this.skininfoPK.equals(other.skininfoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Skininfo[ skininfoPK=" + skininfoPK + " ]";
    }
    
}
