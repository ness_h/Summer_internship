/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author G50
 */
@Embeddable
public class FileinfoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idfile")
    private String idfile;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idmain")
    private String idmain;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idpart")
    private String idpart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idcase")
    private String idcase;

    public FileinfoPK() {
    }

    public FileinfoPK(String idfile, String idmain, String idpart, String idcase) {
        this.idfile = idfile;
        this.idmain = idmain;
        this.idpart = idpart;
        this.idcase = idcase;
    }

    public String getIdfile() {
        return idfile;
    }

    public void setIdfile(String idfile) {
        this.idfile = idfile;
    }

    public String getIdmain() {
        return idmain;
    }

    public void setIdmain(String idmain) {
        this.idmain = idmain;
    }

    public String getIdpart() {
        return idpart;
    }

    public void setIdpart(String idpart) {
        this.idpart = idpart;
    }

    public String getIdcase() {
        return idcase;
    }

    public void setIdcase(String idcase) {
        this.idcase = idcase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfile != null ? idfile.hashCode() : 0);
        hash += (idmain != null ? idmain.hashCode() : 0);
        hash += (idpart != null ? idpart.hashCode() : 0);
        hash += (idcase != null ? idcase.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FileinfoPK)) {
            return false;
        }
        FileinfoPK other = (FileinfoPK) object;
        if ((this.idfile == null && other.idfile != null) || (this.idfile != null && !this.idfile.equals(other.idfile))) {
            return false;
        }
        if ((this.idmain == null && other.idmain != null) || (this.idmain != null && !this.idmain.equals(other.idmain))) {
            return false;
        }
        if ((this.idpart == null && other.idpart != null) || (this.idpart != null && !this.idpart.equals(other.idpart))) {
            return false;
        }
        if ((this.idcase == null && other.idcase != null) || (this.idcase != null && !this.idcase.equals(other.idcase))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.FileinfoPK[ idfile=" + idfile + ", idmain=" + idmain + ", idpart=" + idpart + ", idcase=" + idcase + " ]";
    }
    
}
