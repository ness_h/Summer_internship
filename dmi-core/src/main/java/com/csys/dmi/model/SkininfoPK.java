/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author G50
 */
@Embeddable
public class SkininfoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idfile")
    private String idfile;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idskin")
    private String idskin;

    public SkininfoPK() {
    }

    public SkininfoPK(String idfile, String idskin) {
        this.idfile = idfile;
        this.idskin = idskin;
    }

    public String getIdfile() {
        return idfile;
    }

    public void setIdfile(String idfile) {
        this.idfile = idfile;
    }

    public String getIdskin() {
        return idskin;
    }

    public void setIdskin(String idskin) {
        this.idskin = idskin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfile != null ? idfile.hashCode() : 0);
        hash += (idskin != null ? idskin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SkininfoPK)) {
            return false;
        }
        SkininfoPK other = (SkininfoPK) object;
        if ((this.idfile == null && other.idfile != null) || (this.idfile != null && !this.idfile.equals(other.idfile))) {
            return false;
        }
        if ((this.idskin == null && other.idskin != null) || (this.idskin != null && !this.idskin.equals(other.idskin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.SkininfoPK[ idfile=" + idfile + ", idskin=" + idskin + " ]";
    }
    
}
