/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Formpart")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formpart.findAll", query = "SELECT f FROM Formpart f"),
    @NamedQuery(name = "Formpart.findByIdpart", query = "SELECT f FROM Formpart f WHERE f.idpart = :idpart"),
    @NamedQuery(name = "Formpart.findByIdmain", query = "SELECT f FROM Formpart f WHERE f.idmain = :idmain"),
    @NamedQuery(name = "Formpart.findByName", query = "SELECT f FROM Formpart f WHERE f.name = :name"),
    @NamedQuery(name = "Formpart.findByNameArb", query = "SELECT f FROM Formpart f WHERE f.nameArb = :nameArb")})
public class Formpart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idpart")
    private String idpart;
    @Size(max = 3)
    @Column(name = "idmain")
    private String idmain;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "name_arb")
    private String nameArb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formpart")
    private Collection<Fileinfo> fileinfoCollection;

    public Formpart() {
    }

    public Formpart(String idpart) {
        this.idpart = idpart;
    }

    public String getIdpart() {
        return idpart;
    }

    public void setIdpart(String idpart) {
        this.idpart = idpart;
    }

    public String getIdmain() {
        return idmain;
    }

    public void setIdmain(String idmain) {
        this.idmain = idmain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameArb() {
        return nameArb;
    }

    public void setNameArb(String nameArb) {
        this.nameArb = nameArb;
    }

    @XmlTransient
    public Collection<Fileinfo> getFileinfoCollection() {
        return fileinfoCollection;
    }

    public void setFileinfoCollection(Collection<Fileinfo> fileinfoCollection) {
        this.fileinfoCollection = fileinfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpart != null ? idpart.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formpart)) {
            return false;
        }
        Formpart other = (Formpart) object;
        if ((this.idpart == null && other.idpart != null) || (this.idpart != null && !this.idpart.equals(other.idpart))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Formpart[ idpart=" + idpart + " ]";
    }
    
}
