/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Form")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Form.findAll", query = "SELECT f FROM Form f"),
    @NamedQuery(name = "Form.findByIdmain", query = "SELECT f FROM Form f WHERE f.idmain = :idmain"),
    @NamedQuery(name = "Form.findByName", query = "SELECT f FROM Form f WHERE f.name = :name"),
    @NamedQuery(name = "Form.findByNameArb", query = "SELECT f FROM Form f WHERE f.nameArb = :nameArb")})
public class Form implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idmain")
    private String idmain;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "name_arb")
    private String nameArb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "form")
    private Collection<Fileinfo> fileinfoCollection;

    public Form() {
    }

    public Form(String idmain) {
        this.idmain = idmain;
    }

    public String getIdmain() {
        return idmain;
    }

    public void setIdmain(String idmain) {
        this.idmain = idmain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameArb() {
        return nameArb;
    }

    public void setNameArb(String nameArb) {
        this.nameArb = nameArb;
    }

    @XmlTransient
    public Collection<Fileinfo> getFileinfoCollection() {
        return fileinfoCollection;
    }

    public void setFileinfoCollection(Collection<Fileinfo> fileinfoCollection) {
        this.fileinfoCollection = fileinfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmain != null ? idmain.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Form)) {
            return false;
        }
        Form other = (Form) object;
        if ((this.idmain == null && other.idmain != null) || (this.idmain != null && !this.idmain.equals(other.idmain))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Form[ idmain=" + idmain + " ]";
    }
    
}
