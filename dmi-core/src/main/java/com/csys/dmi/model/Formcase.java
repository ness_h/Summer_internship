/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Formcase")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formcase.findAll", query = "SELECT f FROM Formcase f"),
    @NamedQuery(name = "Formcase.findByIdcase", query = "SELECT f FROM Formcase f WHERE f.idcase = :idcase"),
    @NamedQuery(name = "Formcase.findByIdpart", query = "SELECT f FROM Formcase f WHERE f.idpart = :idpart"),
    @NamedQuery(name = "Formcase.findByName", query = "SELECT f FROM Formcase f WHERE f.name = :name"),
    @NamedQuery(name = "Formcase.findByType", query = "SELECT f FROM Formcase f WHERE f.type = :type"),
    @NamedQuery(name = "Formcase.findByNameArb", query = "SELECT f FROM Formcase f WHERE f.nameArb = :nameArb")})
public class Formcase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "idcase")
    private String idcase;
    @Size(max = 3)
    @Column(name = "idpart")
    private String idpart;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "type")
    private String type;
    @Size(max = 50)
    @Column(name = "name_arb")
    private String nameArb;

    public Formcase() {
    }

    public Formcase(String idcase) {
        this.idcase = idcase;
    }

    public String getIdcase() {
        return idcase;
    }

    public void setIdcase(String idcase) {
        this.idcase = idcase;
    }

    public String getIdpart() {
        return idpart;
    }

    public void setIdpart(String idpart) {
        this.idpart = idpart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNameArb() {
        return nameArb;
    }

    public void setNameArb(String nameArb) {
        this.nameArb = nameArb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcase != null ? idcase.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formcase)) {
            return false;
        }
        Formcase other = (Formcase) object;
        if ((this.idcase == null && other.idcase != null) || (this.idcase != null && !this.idcase.equals(other.idcase))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Formcase[ idcase=" + idcase + " ]";
    }
    
}
