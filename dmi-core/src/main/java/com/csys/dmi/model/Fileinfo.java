/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.dmi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author G50
 */
@Entity
@Table(name = "Fileinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fileinfo.findAll", query = "SELECT f FROM Fileinfo f"),
    @NamedQuery(name = "Fileinfo.findByIdfile", query = "SELECT f FROM Fileinfo f WHERE f.fileinfoPK.idfile = :idfile"),
    @NamedQuery(name = "Fileinfo.findByIdmain", query = "SELECT f FROM Fileinfo f WHERE f.fileinfoPK.idmain = :idmain"),
    @NamedQuery(name = "Fileinfo.findByIdpart", query = "SELECT f FROM Fileinfo f WHERE f.fileinfoPK.idpart = :idpart"),
    @NamedQuery(name = "Fileinfo.findByIdcase", query = "SELECT f FROM Fileinfo f WHERE f.fileinfoPK.idcase = :idcase"),
    @NamedQuery(name = "Fileinfo.findByValue", query = "SELECT f FROM Fileinfo f WHERE f.value = :value")})
public class Fileinfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FileinfoPK fileinfoPK;
    @Size(max = 50)
    @Column(name = "value")
    private String value;
    @JoinColumn(name = "idmain", referencedColumnName = "idmain", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Form form;
    @JoinColumn(name = "idpart", referencedColumnName = "idpart", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Formpart formpart;
    @JoinColumn(name = "idfile", referencedColumnName = "idfile", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Medicalfile medicalfile;

    public Fileinfo() {
    }

    public Fileinfo(FileinfoPK fileinfoPK) {
        this.fileinfoPK = fileinfoPK;
    }

    public Fileinfo(String idfile, String idmain, String idpart, String idcase) {
        this.fileinfoPK = new FileinfoPK(idfile, idmain, idpart, idcase);
    }

    public FileinfoPK getFileinfoPK() {
        return fileinfoPK;
    }

    public void setFileinfoPK(FileinfoPK fileinfoPK) {
        this.fileinfoPK = fileinfoPK;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Formpart getFormpart() {
        return formpart;
    }

    public void setFormpart(Formpart formpart) {
        this.formpart = formpart;
    }

    public Medicalfile getMedicalfile() {
        return medicalfile;
    }

    public void setMedicalfile(Medicalfile medicalfile) {
        this.medicalfile = medicalfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileinfoPK != null ? fileinfoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fileinfo)) {
            return false;
        }
        Fileinfo other = (Fileinfo) object;
        if ((this.fileinfoPK == null && other.fileinfoPK != null) || (this.fileinfoPK != null && !this.fileinfoPK.equals(other.fileinfoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.dmi.model.Fileinfo[ fileinfoPK=" + fileinfoPK + " ]";
    }
    
}
