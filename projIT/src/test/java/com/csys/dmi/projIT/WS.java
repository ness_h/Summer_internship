package com.csys.dmi.projIT;


import java.net.MalformedURLException;
import java.net.URL;
import service.*;


public class WS {

    public static service.PatientWS port;

    public  service.PatientWS WS() throws MalformedURLException {
        service.PatientWS_Service service = new PatientWS_Service(new URL("http://localhost:8080/dmi-core/PatientWS?wsdl"));
         port = service.getPatientWSPort();        
        return port;
    }
}
