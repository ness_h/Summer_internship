<%-- 
    Document   : content
    Created on : 11 juil. 2016, 09:53:20
    Author     : G50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <jsp:include page="../body_page/css_declare.jsp"/>
    <jsp:include page="../body_page/js_declare.jsp"/>
    <script src="../body_page_js/page_content_function.js"></script>
    <script src="../body_page_js/page_content1.js"></script>


    <title>JSP Page</title>
    <style> input[type=checkbox][disabled]{

            outline:1px solid black; 
        }

        div.polaroid {
            width: 100%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            font-size:20px;
        }
        .alert-box {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;  
        }

        .success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            display: none;
        }
    </style>
</head>
<body> 

    <!--alertbox!-->
    <div class="alert-box success">You just added a new Medical File ! <a href="../body_page/content_list.jsp">view list of patients</a></div>
    <!--content of formulaire!-->
    <div class="content"id="cont"> 

        <section id="widget-grid" class="">
            <div class="row col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid">
                    <!-- information générale -->
                    <div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="true"> 
                        <header class="ui-sortable-handle">
                            <span class="widget-icon" id="k"> <i class="fa fa-edit"></i> </span>
                            <h2 id="kk"><strong id="f1"></strong></h2>
                        </header>
                        <div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">   
                                    <form class="form-horizontal  bv-form" novalidate="novalidate" method="post" id="formHaut" name="formHaut">
                                        <fieldset>
                                            <div class="form-group ">
                                                <label class="col-sm-1 control-label pull-left" id="f2"></label>
                                                <div class="col-sm-2 " id="p2">
                                                    <input class="form-control" placeholder="" type="text" id="name" disabled="disabled" name="Name" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                                </div>
                                                <label class="col-sm-1 control-label pull-left" id="f3"></label>
                                                <div class="col-sm-2" id="p3">
                                                    <input type="text" class="form-control" id="medf" name="ID" disabled="disabled" data-mask-placeholder="" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                                </div>

                                                <label class="col-sm-1 control-label pull-left" id="f4"></label>
                                                <div class="col-sm-2" id="p4">
                                                    <input class="form-control" placeholder="" type="text"  disabled="disabled" id="age" name="pouls" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                                </div>


                                                <label class="col-sm-1 control-label pull-left" id="f5"></label>
                                                <div class="col-sm-2" id="p5">
                                                    <div class="input-group">
                                                        <input type="text" id="datepicker" name="dateInterv" placeholder="Choisir date" class="form-control datepicker date-scroll hasDatepicker" data-dateformat="dd/mm/yy" readonly="">
                                                        <span class="input-group-addon"><i class="fa fa-calendar datepicker"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>



                        <div>
                            <form id="formInt" name="formInt" method="post" class="form-horizontal bv-form" novalidate="novalidate">
                                <fieldset>

                                    <div class="form-group">


                                        <label class="col-sm-2 control-label pull-left" id="f6"></label>
                                        <div class="col-sm-2 " id="p6">
                                            <input class="form-control"   min="40" max="100" required type="number" id="pulse"  name="pulse" data-mask-placeholder="" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                        </div>
                                        <label class="col-sm-2 control-label pull-left" id="f7"></label>
                                        <div class="col-sm-2" id="p7">
                                            <input type="text" class="form-control" id="bp" required name="bp" type="text" data-mask-placeholder="" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                        </div>

                                        <label class="col-sm-2 control-label pull-left" id="f8"></label>
                                        <div class="col-sm-2" id="p8">
                                            <input class="form-control" placeholder="" min="10" max="40" required type="number" id="br"  name="br" data-validation="length" data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 50 caractères !">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label pull-left" id="f9"></label>
                                        <div class="col-sm-2" id="p9">
                                            <input type="number" class="form-control" id="temp" name="temp"  data-mask-placeholder="" data-validation="length" min="36" max="38" required data-validation-length="max50" data-validation-error-msg="Ce champs ne doit pas dépasser 3 caractères !">
                                        </div>

                                        <label class="col-sm-2 control-label pull-left" id="f10"></label>
                                        <div class="col-sm-2" id="p10">
                                            <select class="form-control" id="pr">
                                                <option selected="selected" value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>

                                            </select></div>
                                        <label class="col-sm-2 control-label pull-left" id="f11"></label>
                                        <div class="col-sm-2 " id="p11">
                                            <input type="number" class="form-control "required  min="2" max="600" id="Weight" name="Weight"  data-mask-placeholder="" data-validation="length" data-validation-length="max3" data-validation-error-msg="Ce champs ne doit pas dépasser 3 caractères !">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                  
                                    <div>
                                        <legend ><i id="f12"> </i></legend>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 has-success">
                                                <textarea id="mdiag" name="txtareaAutreCarVas" required class="myclassAutre form-control valid" spellcheck="false" row="1" data-validation="length" data-validation-length="max500" data-validation-error-msg="Ce champs ne doit pas dépasser 500 caractères !" data-autosize-on="true" ></textarea>
                                            </div>
                                        </div>
                                    </div>


                                </fieldset>
                            </form>
                        </div>
                    </div></div>
            </div>  </section>
    </div>    
 <!--end of formulaire!-->
 <!--fiche checkboxes!-->
    <div  class=" col col-12"style="height:1700px" id='dyn'></div> 
 <!--end fiche!-->



 <!--footer!-->
<div class="page-footer footerParent fixed-page-footer" id="footerParent" style="position: fixed;">
    <div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <button type="button" class="btn btn-labeled btn-default" id="printt" style="margin-top:-12px;margin-right:6px;height:40px;margin-left:20px;">
                <span class="btn-label">
                    <i class="glyphicon glyphicon-print"></i>
                </span>
                Print
            </button>
            <button type="submit" class="btn btn-success" id="MyButton" style="margin-top:-12px;height:40px;margin-left:20px;width:120px;">
                <span class="btn-label btn-success">
                    <i class="glyphicon glyphicon-ok"></i>
                </span>
                Validate
            </button>
            <button type="button" class="btn btn-danger btn-labeled" id="closed" style="margin-top:-12px;margin-right:6px;height:40px;margin-left:20px;">
                <span class="btn-label">
                    <i class="fa fa-times"></i>
                </span>
                Close
            </button>
        </div>
    </div> 
</div>
 <!--end footer!-->

</body>
