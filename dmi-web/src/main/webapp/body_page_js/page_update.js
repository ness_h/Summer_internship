/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    var patinfo=findnameforid(sessionStorage.getItem("idpat"));
    $('#id').val(patinfo[0].idpatient);
    $('#name').val(patinfo[0].name);
    $('#dateu').val(patinfo[0].birthdate);
    $('#adr').val(patinfo[0].adress);
    $('#sex').val(patinfo[0].sexe);
    $('#phone').val(patinfo[0].phonenumber);
    $('#idcard').val(patinfo[0].cin);
            
        $('#MyButtonupdate').click(function (e) {

        var name = document.getElementById('name').value;
        var dateInterv = document.getElementById('dateu').value;
        var idcard = document.getElementById('idcard').value;
        var phone = document.getElementById('phone').value;
        var adr = document.getElementById('adr').value;
        var sex =  document.getElementById('sex').value;
        if (idcard.length < 13) {
            e.preventDefault();
        } else {

            update(sessionStorage.getItem("idpat"), name, dateInterv, adr, sex, phone, idcard);
            showmsg();
            //window.location.replace("../body_page/content_list.jsp");
            return false;
        }

    });       
    
});

function showmsg() {
    $("div.success").fadeIn(300).delay(4000).fadeOut(400);
}