package com.csys.dmi.servlet;

import com.csys.dmi.generic.WS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

public class Add_p extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, java.text.ParseException, DatatypeConfigurationException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new GsonBuilder().serializeNulls().create();

            // HttpSession session = request.getSession(true);
            WS.portPatientWS = null;
            String function = request.getParameter("function");
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            String sex = request.getParameter("sex");
            String adr = request.getParameter("adr");
            String phone = request.getParameter("phone");
            String idcard = request.getParameter("idcard");
            String dateInterv = request.getParameter("dateInterv");
            String idmain = request.getParameter("idmain");
            String idpart = request.getParameter("idpart");
            String idcase = request.getParameter("idcase");
            String idfile = request.getParameter("idfile");
            String value = request.getParameter("value");
            String idpatient = request.getParameter("idpatient");
            String pulse = request.getParameter("pulse");
            String respiratoryrate = request.getParameter("respiratoryrate");
            String bloodpressure = request.getParameter("bloodpressure");
            String bodytemperature = request.getParameter("bodytemperature");
            String painrate = request.getParameter("painrate");
            String weight = request.getParameter("weight");
            String diagnostic = request.getParameter("diagnostic");
            String idskin = request.getParameter("idskin");
            String place = request.getParameter("place");
            String size = request.getParameter("size");
            String color = request.getParameter("color");
            String secretions = request.getParameter("secretions");
            //String state = request.getParameter("state");
             int temp;
             int pr;
             int pul;
             int rr;

            //DateFormat df1 = new SimpleDateFormat("yyyy-mm-dd");
            //Date d = new Date();
            javax.xml.datatype.XMLGregorianCalendar dateu;
            GregorianCalendar c11 = new GregorianCalendar();

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dtt;
            //if ("null".equals(dateu)) {
                dtt = new Date();
//            } else {
//                dtt = sdf.parse(dateu,new ParsePosition(5));
//            }
            
            WS webService = new WS();
            webService.PatientWS();

            switch (function) {
                case "getlistpatient":
                    out.println(gson.toJson(WS.portPatientWS.getlistpatient()));
                    break;
                case "findnameforid":
                    out.println(gson.toJson(WS.portPatientWS.findnameforid(id)));
                    break;
                case "insert":
                     dtt = sdf.parse(dateInterv);
                     System.out.println("Date in dd-MMM-yyyy format is: "+sdf.format(dtt));
                    // d = df1.parse(dateu);
                    c11.setTime(dtt);
                    dateu = DatatypeFactory.newInstance().newXMLGregorianCalendar(c11);

                    WS.portPatientWS.insert(id, name, dateu, adr, sex, phone, idcard);
                    break;
                case "getnamemain":
                    out.println(gson.toJson(WS.portPatientWS.getnamemain(idmain)));
                    break;
                case "getnamepart":
                    out.println(gson.toJson(WS.portPatientWS.getnamepart(idpart)));
                    break;
                case "getnamecase":
                    out.println(gson.toJson(WS.portPatientWS.getnamecase(idcase)));
                    break;
                case "getlistmain":
                    out.println(gson.toJson(WS.portPatientWS.getlistmain()));
                    break;
                case "getlistfiles":
                    out.println(gson.toJson(WS.portPatientWS.getlistfiles()));
                    break;
                case "painrate":
                 
                    if (painrate != null) {
                        pr = Integer.parseInt(painrate);
                    } else {
                        pr = 0;
                    }
                    out.println(gson.toJson(WS.portPatientWS.painrate(pr)));
                    break;
                case "getlistpart":
                    out.println(gson.toJson(WS.portPatientWS.getlistpart(idmain)));
                    break;
                case "getlistcase":
                    out.println(gson.toJson(WS.portPatientWS.getlistcase(idpart)));
                    break;
                case "getlistinfo":
                    out.println(gson.toJson(WS.portPatientWS.getlistinfo(idfile)));
                    break;
                case "insertfile":
                    WS.portPatientWS.insertfile(idfile, idmain, idpart, idcase, value);
                    break;
                case "gettypecase":
                    out.println(gson.toJson(WS.portPatientWS.gettypecase(idcase)));
                    break;
                case "getmain":
                    out.println(gson.toJson(WS.portPatientWS.getmain(idpart)));
                    break;
                case "getallcase":
                    out.println(gson.toJson(WS.portPatientWS.getallcase()));
                    break;
                case "getallpart":
                    out.println(gson.toJson(WS.portPatientWS.getallpart()));
                    break;
                case "getlistfilesbyidpatient":
                    out.println(gson.toJson(WS.portPatientWS.getlistfilesbyidpatient(idpatient)));
                    break;
                case "getlistfilesbyidfile":
                    out.println(gson.toJson(WS.portPatientWS.getlistfilesbyidfile(idfile)));
                    break;
                case "insertmf":
                    dtt = sdf.parse(dateInterv);
                    c11.setTime(dtt);
                    dateu = DatatypeFactory.newInstance().newXMLGregorianCalendar(c11);

                   
                    
                    int wght;
                    if (bodytemperature != null) {
                        temp = Integer.parseInt(bodytemperature);
                    } else {
                        temp = 0;
                    }
                    if (painrate != null) {
                        pr = Integer.parseInt(painrate);
                    } else {
                        pr = 0;
                    }
                    if (weight != null) {
                        wght = Integer.parseInt(weight);
                    } else {
                        wght = 0;
                    }
                    if (pulse != null) {
                        pul = Integer.parseInt(pulse);
                    } else {
                        pul = 0;
                    }
                    if (respiratoryrate != null) {
                        rr = Integer.parseInt(respiratoryrate);
                    } else {
                        rr = 0;
                    }
                    WS.portPatientWS.insertmf(idpatient, idfile, dateu, pul, rr, bloodpressure, temp, pr, wght, diagnostic);
                    break;
                case "insertskin":
                    WS.portPatientWS.insertskin(idfile, idskin, place, size, color, secretions);
                    break;
                case "getlistskin":
                    out.println(gson.toJson(WS.portPatientWS.getlistskin(idfile)));
                    break;
                case "getusers":
                    out.println(gson.toJson(WS.portPatientWS.getusers()));
                    break;
                case "todayspat":
                    dtt = sdf.parse(dateInterv);
                    c11.setTime(dtt);
                    dateu = DatatypeFactory.newInstance().newXMLGregorianCalendar(c11);
                    out.println(gson.toJson(WS.portPatientWS.todayspat(dateu)));
                    break;
                case "update":
                      dtt = sdf.parse(dateInterv);
                    // System.out.println("Date in dd-MMM-yyyy format is: "+sdf.format(dtt));
                    // d = df1.parse(dateu);
                    c11.setTime(dtt);
                    dateu = DatatypeFactory.newInstance().newXMLGregorianCalendar(c11);

                    WS.portPatientWS.update(id, name, dateu, adr, sex, phone, idcard);
                    break;
                default:
                    break;
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (java.text.ParseException | DatatypeConfigurationException ex) {
            Logger.getLogger(Add_p.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (java.text.ParseException | DatatypeConfigurationException ex) {
            Logger.getLogger(Add_p.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
