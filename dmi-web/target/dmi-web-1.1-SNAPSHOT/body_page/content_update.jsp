<%-- 
    Document   : contact_add
    Created on : 11 juil. 2016, 10:22:29
    Author     : G50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="../body_page/css_declare.jsp"/>
        <jsp:include page="../body_page/js_declare.jsp"/>
        <script src="../body_page_js/page_function.js"></script>
        <script src="../body_page_js/page_update.js"></script>

        <title>JSP Page</title>
        <style> div.polaroid {

                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

            }
            .alert-box {
	padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;  
}

.success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    display: none;
}

        </style>
    </head>
    <body>

        <div class="alert-box success">You just updated a patient ! <a href="../body_page/content_list.jsp">view list of patients</a></div>
    <tr>


        <td colspan="6" class="text" valign="top" ><div align="justify" >
                <div class="polaroid" style="width:800px;margin: auto;">
                    <div class="jarviswidget " style="width:800px;margin: auto;" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                        <header role="heading" style="background-color:#DCDCDC; border-color: #C0C0C0;"><div class="jarviswidget-ctrls" role="menu">  </a></div><span class="widget-icon">   <img src="../img/icon/glyphicons-151-edit.png "style="width:20px;height:20px;"></span>
                            <h2>Basic Patient Informations</h2>				

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <div class="well" style="padding-top:40px;">

                            <form class="form-inline" id="formu" style="padding-left:20px;">
                                <div class="form-group">
                                    <label for="exampleInputName2">ID</label><br><br>
                                    <input type="text" class="form-control" id="id" style="background-color: #DCDCDC	"placeholder="Patient's ID" required maxlength="3">

                                </div><br><br><br>
                                <div class="form-group">
                                    <label for="name">Name</label><br><br>
                                    <input type="text" class="form-control" id="name"  required>
                                  
                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="minDate">Birthday</label><br><br>
                                    <input type="text" class="form-control" id="dateu" value=""  required>
                                    
                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <div class="form-group">
                                    <label for="exampleInputName2">Address</label><br><br>

                                    <input id="adr" name="address-line1" type="text" 
                                           class="form-control" required>
                                  
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="exampleInputName2" >SEX</label><br><br>
                                    <input id="sex" name="address-line1" type="text" 
                                           class="form-control" required>


                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="exampleInputName2">Phone Number</label><br><br>

                                    <input id="phone" name="address-line1" type="text"
                                           class="form-control" maxlength="14" required>
                                   
                                </div>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="exampleInputName2">Identity Card</label><br><br>

                                    <input id="idcard" name="address-line1" type="text" 
                                           class="form-control"  minlength="13" maxlength="14" required>
                                 
                                </div><br><br><br>

                                <button type="submit" id="MyButtonupdate" class="btn btn-primary pull-right"  >Update Patient</button>
                                <br><br><br>
                            </form> </div>  </div></div></td>


    
</tr>


</body>
</html>
