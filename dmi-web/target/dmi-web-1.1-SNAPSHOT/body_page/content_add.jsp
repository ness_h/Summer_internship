<%-- 
    Document   : contact_add
    Created on : 11 juil. 2016, 10:22:29
    Author     : G50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="../body_page/css_declare.jsp"/>
        <jsp:include page="../body_page/js_declare.jsp"/>
        <script src="../body_page_js/page_list_function.js"></script>
        <script src="../body_page_js/page_list.js"></script>

        <title>JSP Page</title>
        <style> div.polaroid {

                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

            }
            .alert-box {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;  
            }

            .success {
                color: #3c763d;
                background-color: #dff0d8;
                border-color: #d6e9c6;
                display: none;
            }

        </style>
    </head>
    <body>
        <!--alertbox!-->
        <div class="alert-box success">You just added a new patient ! <a href="../body_page/content_list.jsp">view list of patients</a></div>


    <tr>


        <td colspan="6" class="text" valign="top" ><div align="justify" >
                <div class="polaroid" style="width:800px;margin: auto;">
                    <div class="jarviswidget " style="width:800px;margin: auto;" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                        <header role="heading" style="background-color:#DCDCDC; border-color: #C0C0C0;"><div class="jarviswidget-ctrls" role="menu">  </a></div><span class="widget-icon">   <img src="../img/icon/glyphicons-151-edit.png "style="width:20px;height:20px;"></span>
                            <h2>Basic Patient Informations</h2>				

                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                        <div class="well" style="padding-top:40px;">

                            <form class="form-inline" id="formu" style="padding-left:20px;">
                                <div class="form-group">
                                    <label for="exampleInputName2">ID</label><br><br>
                                    <input type="text" class="form-control" id="id" style="background-color: #DCDCDC	"placeholder="Patient's ID" required maxlength="3">

                                </div><br><br><br>
                                <div class="form-group">
                                    <label for="name">Name</label><br><br>
                                    <input type="text" class="form-control" id="name" placeholder="Patient's Name" required>
                                    <p class="help-block"style="font-size:10px">John Marcus Snow</p>
                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="minDate">Birthday</label><br><br>
                                    <input type="text" class="form-control" id="dateu" value="" placeholder="yyyy-MM-dd" required>
                                    <p class="help-block"style="font-size:10px">1989-06-05</p>
                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <div class="form-group">
                                    <label for="exampleInputName2">Address</label><br><br>

                                    <input id="adr" name="address-line1" type="text" placeholder="Patient's Adress"
                                           class="form-control" required>
                                    <p class="help-block"style="font-size:10px">Street address, P.O. box, company name, c/o</p>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="exampleInputName2" >SEX</label><br><br>
                                    <select class="form-control" id="sex">
                                        <option selected="selected" value="male">MAN</option>
                                        <option value="female">WOMAN</option>

                                    </select>


                                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="exampleInputName2">Phone Number</label><br><br>

                                    <input id="phone" name="address-line1" type="text" placeholder="Patient's Phone Number"
                                           class="form-control" maxlength="14" required>
                                    <p class="help-block"style="font-size:10px">0020 11xxxxxxxx</p>
                                </div>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="form-group">
                                    <label for="exampleInputName2">Identity Card</label><br><br>

                                    <input id="idcard" name="address-line1" type="text" placeholder="Patient's ID Card"
                                           class="form-control"  minlength="13" maxlength="14" required>
                                    <p class="help-block"style="font-size:10px">12xxxxxxxxxxxx</p>
                                </div><br><br><br>

                                <button type="submit" id="MyButtonadd" class="btn btn-primary pull-right"  >Add Patient</button>
                                <br><br><br>
                            </form> </div>  </div></div></td>



    </tr>


</body>
</html>
