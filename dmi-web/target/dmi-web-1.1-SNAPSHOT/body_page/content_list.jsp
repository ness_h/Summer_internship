<%-- 
    Document   : content_list
    Created on : 11 juil. 2016, 11:37:46
    Author     : G50
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <jsp:include page="../body_page/js_declare.jsp"/>

        <script src="../body_page_js/page_list_function.js"></script>
        <script src="../body_page_js/page_list.js"></script>
        <script src="../js/jquery-1.12.3.js"></script>
        <jsp:include page="../body_page/css_declare.jsp"/>

        <script src="../js/bootstrap/bootstrap.min.js"></script>
        <script src="../js/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables.bootstrap.min.js"></script>

    </head>
    <body>
        <!-- <div id="example_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="example_length">
                                          <label>Show <select name="example_length" aria-controls="example" class=""><option value="10">10</option>
                                                  <option value="25">25</option><option value="50">50</option><option value="100">100</option>
                                              </select> entries</label></div><div id="example_filter" class="dataTables_filter">
                                                  <label>Search:<input type="search" class="" placeholder="" aria-controls="example"></label>
                                              </div>-->
        <article class="col-sm-12 sortable-grid ui-sortable">
            <table id="example" class="table table-striped table-bordered " cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Birthdate</th>
                        <th>Adress</th>
                        <th>Sexe</th>
                        <th>Phone number</th>
                        <th>ID card</th>
                        <th ></th>

                    </tr>
                </thead>

                <tbody>

                <script>PrintResults();</script>

                </tbody>
            </table>
        </article>
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });

        </script>
    </body>
</html>
