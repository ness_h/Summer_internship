/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global idpat */


$(document).ready(function () {

    sessionStorage.removeItem("idfile");
//to get ID files generated automatically
    var list_number = getlistfiles();
    var max = "100";
    for (var i = 0; i < list_number.length; i++) {
        max = list_number[i].idfile;
    }
    pageID = parseInt(max) + 1;
    nxtID = "" + pageID;
    sessionStorage.setItem("idfile", "" + pageID);
    var patinfo = findnameforid(sessionStorage.getItem("idpat"));
    //to set values of static form
    var _id = "datepicker";
    setInputDate("#datepicker");
    $('#medf').val(sessionStorage.getItem("idfile"));
    $('#age').val(getAge(patinfo[0].birthdate));
    $('#name').val(patinfo[0].name);
//to charge names from css file 
    if (sessionStorage.getItem("lang") === "Eng") {

        var url = "../css/css_eng.css",
                head = document.getElementsByTagName('head')[0];
        link = document.createElement('link');
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = url;
        head.appendChild(link);
        var z = document.getElementById("cont");
        z.dir = "rtc";
    } else {
        var url = "../css/css_arb.css",
                head = document.getElementsByTagName('head')[0];
        link = document.createElement('link');
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = url;
        head.appendChild(link);
        var z = document.getElementById("cont");
        z.dir = "rtl";
        document.getElementById('k').classList.add('pull-right');
        document.getElementById('k').style.marginRight = "5px";
        document.getElementById('kk').classList.add('pull-right');
        for (var i = 2; i < 12; i++) {
            document.getElementById('f' + i).classList.remove('pull-left');
            document.getElementById('f' + i).classList.add('pull-right');
        }
        for (var i = 2; i < 12; i++) {
            document.getElementById('p' + i).classList.add('pull-right');
        }
    }

//to draw dynamic formulaire 
    $('#dyn').append('<section id="widget-grid" class=""><div class="row col-lg-12">');
    //variables to get words of dynamic formulaire from database
    var list_main = getlistmain();
    var list_part1 = getallpart();
    var list_case1 = getallcase();
    var typ_name = "name";
    if (sessionStorage.getItem("lang") === "Arb") {
        typ_name = "nameArb";
    }


//drawing the form  
    for (var i = 0; i < list_main.length; i++) {

        if (sessionStorage.getItem("lang") === "Eng") {
            $('#dyn').append('<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sortable-grid" style="height:600pxpx;font-size:90%;">' +
                    '<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="true">' +
                    '<header class="ui-sortable-handle"><span class="widget-icon"> <i class="fa fa-edit"></i> </span>&nbsp;<h2><strong>' + (list_main[i])[typ_name] + '</strong></h2></header>' +
                    '<div><div class="row" role="content" style="height:600px;" ><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><form class="form-horizontal has-validation-callback" method="post" id ="' + list_main[i].idmain + '" name="formHaut" >' +
                    '</form></div></div></div></div></article>');
        }
//to inverse direction of page
        if (sessionStorage.getItem("lang") === "Arb") {
            $('#dyn').append('<article dir="rtl" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sortable-grid" style="font-size:100%;">' +
                    '<div dir="rtl" class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="true">' +
                    '<header  class="ui-sortable-handle"><span class="widget-icon pull-right" style="margin-right:5px"> <i class="fa fa-edit"></i> </span>&nbsp;<h2 class="pull-right"><strong>' + list_main[i].nameArb + '</strong></h2></header>' +
                    '<div><div dir="rtl" class="row" role="content" style="height:500px;" ><div dir="rtl" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><form class="form-horizontal has-validation-callback" method="post" id ="' + list_main[i].idmain + '" name="formHaut" >' +
                    '</form></div></div></div></div></article>');
        }


        $('#' + list_main[i].idmain).append('<fieldset><section  ><div class="row"><div class="" style="padding-left:50px" id ="i' + list_main[i].idmain + '">');
        //get list_parts of specefic main variable
        var list_part = [];
        $.each(list_part1, function () {
            if (this.idmain === list_main[i].idmain
                    ) {
                list_part.push(this);
            }
        });
        if (list_part !== null) {
            for (var j = 0; j < list_part.length; j++) {
//get list_cases of specefic part variable
                var list_case = [];
                $.each(list_case1, function () {
                    if (this.idpart === list_part[j].idpart
                            ) {
                        list_case.push(this);
                    }
                });
                //if part has no cases then he's checkbox
                if (list_case.length === 0) {
                    $('#i' + list_main[i].idmain).append('<div class="col-lg-6"><label  class="checkbox"><input type="checkbox" id="' + list_part[j].idpart + '" value="option1" >&nbsp;&nbsp;&nbsp;&nbsp;' + (list_part[j])[typ_name] + '</label></div>');
                }
//drawing the cases (suivant leur types)
                if (list_case.length !== 0) {
                    $('#i' + list_main[i].idmain).append('<div class="col-lg-6"><label   ><p id="' + list_part[j].idpart + '" >' + (list_part[j])[typ_name] + '</p><div id="j' + list_part[j].idpart + '"></div><br></label></div>');
                    for (var k = 0; k < list_case.length; k++) {

                        if ((list_case[k].type) === null) {
                            $('#j' + list_part[j].idpart).append('<label class="radio-inline" ><input type="radio" id="' + list_case[k].idcase + '" value="option1" class="onlyone" name="' + list_part[j].idpart + '">&nbsp;&nbsp;&nbsp;&nbsp;<i >' + (list_case[k])[typ_name] + '</i>&nbsp;');
                            $('#j' + list_part[j].idpart).append('</label>');
                        }
                        if ((list_case[k].type) === "check") {
                            if (list_case[k].idcase === "c1i") {
                                $('#j' + list_part[j].idpart).append('<label class="radio-inline" ><input type="radio"  class="onlyone" id="' + list_case[k].idcase + '" value="option1" name="' + list_part[j].idpart + '">&nbsp;&nbsp;&nbsp;&nbsp;<i >' + (list_case[k])[typ_name] + '</i>&nbsp;<div id="container_add"></div></label>');
                            } else if (list_case[k].idcase === "c2l") {
                                $('#j' + list_part[j].idpart).append('<label class="radio-inline" ><input type="radio"  class="onlyone" id="' + list_case[k].idcase + '" value="option1" name="' + list_part[j].idpart + '">&nbsp;&nbsp;&nbsp;&nbsp;<i>' + (list_case[k])[typ_name] + '</i>&nbsp;<div id="container_add3"></div></label>');
                            } else {
                                $('#j' + list_part[j].idpart).append('<label class="radio-inline" ><input type="radio"  class="onlyone" id="' + list_case[k].idcase + '" value="option1" name="' + list_part[j].idpart + '">&nbsp;&nbsp;&nbsp;&nbsp;<i>' + (list_case[k])[typ_name] + '</i>&nbsp;<div id="container_add2"></div></label>');
                            }

                        }


                    }
                }
            }
            $('#' + list_main[i].idmain).append('</div></div></fieldset>');
        }

    }

    $('#dyn').append('</div></section>');
    //end of form
    //drawing dynamic table of form
    $('#i03').append('<div class="col-md-12 column" id="tabeu"><table class="table table-bordered table-hover" id="tab_logic">');
    $('#tab_logic').append('<thead><tr id="tableu">');
    $('#tableu').append('<th class="text-center">#</th>');
    var list_table = getlistcase("03b");
    for (var e = 0; e < list_table.length; e++) {
        $('#tableu').append('<th class="text-center"  id="' + list_table[e].idcase + '">' + (list_table[e])[typ_name] + '</th>');
    }
    $('#tab_logic').append('</tr></thead>');
    if (sessionStorage.getItem("lang") === "Eng") {
        $('#tab_logic').append('<tbody><tr id="addr0"><td id="1">1</td><td><input type="text" name="place0" id="place0" placeholder="Place" class="form-control"/></td>' +
                '<td><input type="text" name="size0" id="size0" placeholder="Size" class="form-control"/></td>' +
                '<td><input type="text" name="color0" id="color0" placeholder="Color" class="form-control"/></td>' +
                '<td><input type="text" name="secretions0"  id="secretions0" placeholder="Secretions" class="form-control"/></td>' +
                '</tr><tr id="addr1"></tr></tbody>');
        $('#03b').append('</table>');
        $('#tabeu').append('<br><br><a id="add_row" class="btn btn-default pull-left">Add New Row</a><br><br><a id="delete_row" class="pull-left btn btn-default">Delete Last Row</a></div>');
    } else {
        $('#tab_logic').append('<tbody><tr id="addr0"><td id="1">1</td><td><input type="text" name="place0" id="place0" placeholder="المكان" class="form-control"/></td>' +
                '<td><input type="text" name="size0" id="size0" placeholder="الحجم" class="form-control"/></td>' +
                '<td><input type="text" name="color0" id="color0" placeholder="اللون" class="form-control"/></td>' +
                '<td><input type="text" name="secretions0"  id="secretions0" placeholder="الافرازات" class="form-control"/></td>' +
                '</tr><tr id="addr1"></tr></tbody>');
        $('#03b').append('</table>');
        $('#tabeu').append('<br><br><a id="add_row" class="btn btn-default pull-right">إضافة صف جديد</a><br><br><a id="delete_row" class="pull-right btn btn-default">حذف آخر صف</a></div>');
    }
    $('#03b').append('</div></div>');
    //if u click these checkboxes other inputs come out 
    $('#c2l').click(function () {
        if (this.checked) {
            addFields3("d2l");
        }
    });
    $('#c1i').click(function () {
        if (this.checked) {
            addFields2("d1i", "e1i");
        }
    });
    $('#c6c').click(function () {
        if (this.checked) {
            addFields("d6c");
        }
    });
    $('#b6c').click(function () {
        if (this.checked) {
            addFields("d6c");
        }
    });
    //functions to draw dynamic inputs
    function addFields(id) {
        if (sessionStorage.getItem("lang") === "Eng") {
            $('#container_add2').append('<p>Kind of machines?  <br><label for="p_scnts"><input type="text" id=' + id + ' size="30" name="yrs" value=""class="form-control" placeholder="Kind of machines used" /></label></p>');
        } else
            $('#container_add2').append('<p>  <br><label for="p_scnts"><input type="text" id=' + id + ' size="30" name="yrs" value=""class="form-control" placeholder="حدد النوع" /></label></p>');
    }
    function addFields2(id1, id2) {
        if (sessionStorage.getItem("lang") === "Eng") {
            $('#container_add').append('<p>Number of years <br><label for="p_scnts"><input type="number" id=' + id1 + ' size="30" name="yrs" value="" placeholder="Number of years"class="form-control" /></label></p>');
            $('#container_add').append('<p>Number of Cigarettes per day : <br><label for="p_scnts"><input type="number" id=' + id2 + ' size="30" name="nb" value="" placeholder="Number of cigarettes" class="form-control"/></label></p>');
        } else {
            $('#container_add').append('<p>عدد السنوات<br><label for="p_scnts"><input type="number" id=' + id1 + ' size="30" name="yrs" value="" placeholder="عدد السنوات"class="form-control" /></label></p>');
            $('#container_add').append('<p>عدد السجائر اليومي<br><label for="p_scnts"><input type="number" id=' + id2 + ' size="30" name="nb" value="" placeholder="عدد السجائر اليومي" class="form-control"/></label></p>');
        }
    }
    function addFields3(id) {
        if (sessionStorage.getItem("lang") === "Eng") {
            $('#container_add3').append(' <br><label for="p_scnts"><input type="text" id="yrs" size="30" name=' + id + ' value=""class="form-control" placeholder="others.." /></label>');
        } else
            $('#container_add3').append(' <br><label for="p_scnts"><input type="text" id="yrs" size="30" name=' + id + ' value=""class="form-control" placeholder="others.." /></label>');
    }

//to build table
    var i = 1;
    $("#add_row").click(function () {
        if (sessionStorage.getItem("lang") === "Eng") {
            $('#addr' + i).html("<td id=" + (i + 1) + ">" + (i + 1) + "</td><td><input name='place" + i + "' name='place" + i + "' type='text' placeholder='Place' class='form-control input-md'  /> </td><td><input  name='size" + i + "' id='size" + i + "' type='text' placeholder='Size'  class='form-control input-md'></td><td><input  name='color" + i + "' id='color" + i + "' type='text' placeholder='Color'  class='form-control input-md'></td></td><td><input name='secretions" + i + "' id='secretions" + i + "' type='text' placeholder='Secretions' class='form-control input-md'  /> </td>");
        } else {
            $('#addr' + i).html("<td id=" + (i + 1) + ">" + (i + 1) + "</td><td><input name='place" + i + "' name='place" + i + "' type='text' placeholder='المكان' class='form-control input-md'  /> </td><td><input  name='size" + i + "' id='size" + i + "' type='text' placeholder='الحجم'  class='form-control input-md'></td><td><input  name='color" + i + "' id='color" + i + "' type='text' placeholder='اللون'  class='form-control input-md'></td></td><td><input name='secretions" + i + "' id='secretions" + i + "' type='text' placeholder='الافرازات' class='form-control input-md'  /> </td>");
        }
        $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
        i++;
    });
    $("#delete_row").click(function () {
        if (i > 1) {
            $("#addr" + (i - 1)).html('');
            i--;
        }
    });

//one checkbox must be checked
    $('.onlyone').click(function () {
        $('input[name="' + this.name + '"]').prop("checked", false);
        this.checked = true;
    });
//to close page       
    $('#closed').click(function () {
        window.location.replace("../body_page/content_list.jsp");
    });
//submit button
    $('#MyButton').click(function () {
        if ((checktemp() === true) && (checkpulse() === true) && (checkWeight() === true) && (checkbr() === true)) {
            //insert static infos
            insertinfo(sessionStorage.getItem("idpat"), sessionStorage.getItem("idfile"), $('#datepicker').val(), $('#pulse').val(), $('#br').val(), $('#bp').val(), $('#temp').val(), $('#pr').val(), $('#Weight').val(), $('#mdiag').val());
            //insert table infos
            inserttable(sessionStorage.getItem("idfile"), (i - 1));
            //insert checked infos
            validatecheck(sessionStorage.getItem("idfile"));
            showmsg();
            window.location.replace("../body_page/content_list.jsp");
            //return false;
        }
    });
    function showmsg() {
        $("div.success").fadeIn(300).delay(4000).fadeOut(400);
    }
    window.parent.$("#eng").click(function () {
        history.go(0);
    });
    window.parent.$("#arb").click(function () {
        history.go(0);
    });
});

