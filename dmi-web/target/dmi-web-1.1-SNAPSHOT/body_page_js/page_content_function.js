/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getlistfiles()
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistfiles",
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getlistfilesbyidfile(idfile)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistfilesbyidfile&idfile=" + idfile,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function findnameforid(id)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=findnameforid&id=" + id,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getlistmain()
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistmain",
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getallcase()
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getallcase",
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getallpart()
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getallpart",
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getlistcase(idpart)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistcase&idpart=" + idpart,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getlistskin(idfile)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistskin&idfile=" + idfile,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getlistinfo(idfile)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getlistinfo&idfile=" + idfile,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function insertskin(idfile, idskin, place, size, color, secretions)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=insertskin&idfile=" + idfile + "&idskin=" + idskin + "&place=" + place + "&size=" + size + "&color=" + color + "&secretions=" + secretions,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function insertmf(idpatient, idfile, dateInterv, pulse, respiratoryrate, bloodpressure, bodytemperature, painrate, weight, diagnostic)

{
    var reponse;
    $.ajax({
        url: "../Add_p?function=insertmf&idpatient=" + idpatient + "&idfile=" + idfile + "&dateInterv=" + dateInterv + "&pulse=" + pulse + "&respiratoryrate=" + respiratoryrate + "&bloodpressure=" + bloodpressure + "&bodytemperature=" + bodytemperature + "&painrate=" + painrate + "&weight=" + weight + "&diagnostic=" + diagnostic,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function getmain(idpart)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=getmain&idpart=" + idpart,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
function insertfile(idfile, idmain, idpart, idcase, value)
{
    var reponse;
    $.ajax({
        url: "../Add_p?function=insertfile&idfile=" + idfile + "&idmain=" + idmain + "&idpart=" + idpart + "&idcase=" + idcase + "&value=" + value,
        type: 'POST',
        async: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown)
        {
        },
        success: function (data, textStatus, jqXHR)
        {
            reponse = data;
        }
    });

    return reponse;
}
//************** other functions
function checktemp(){
        var bld = $("#temp"),
            val = bld.val(),
            min = bld.attr("min"),
            max = bld.attr("max");
                
        if ( (val>min) && (val <= max) ){
           return true;
        }else{
            bld.val("");
            document.getElementById("temp").style.borderColor = "red";
            alert("Value of body temperature must be between 36 and 38");
            
            return false;
        }
    } 
function checkpulse(){
    var bld = $("#pulse"),
            val = bld.val(),
            min = bld.attr("min"),
            max = bld.attr("max");
                
        if ( (val>min) ){
           return true;
        }else{
            bld.val("");
            document.getElementById("pulse").style.borderColor = "red";
            alert("Value of pulse must be between 40 and 100");
            
            return false;
        }
}
function checkWeight(){
    var bld = $("#Weight"),
            val = bld.val(),
            min = bld.attr("min"),
            max = bld.attr("max");
                
        if ( (val>min) && (val <= max) ){
           return true;
        }else{
            bld.val("");
            document.getElementById("Weight").style.borderColor = "red";
            alert("Value of Weight must be between 2 and 600");
            
            return false;
        }
}
function checkbr(){
    var bld = $("#br"),
            val = bld.val(),
            min = bld.attr("min"),
            max = bld.attr("max");
                
        if ( (val>min) && (val <= max) ){
           return true;
        }else{
            bld.val("");
            document.getElementById("br").style.borderColor = "red";
            alert("Value of breathing rate must be between 10 and 40");
            return false;
        }
}
function inserttable(idfile, i) {
        for (var j = 0; j < i; j++) {
            if(typeof $('#place'+j).val() !== 'undefined' ){
            var place = $('#place' + j).val();
            var size = $('#size' + j).val();
            var color = $('#color' + j).val();
            var secretions = $('#secretions' +j).val();
            insertskin(idfile, j, place, size, color, secretions);
        }}
    }
    //functions to test 

    function insertinfo(idpatient, idfile, date, pulse, respiratoryrate, bloodpressure, bodytemperature, painrate, weight, diagnostic) {
        insertmf(idpatient, idfile, date, pulse, respiratoryrate, bloodpressure, bodytemperature, painrate, weight, diagnostic);

    }
    function validatecheck(idfile) {
        var list_all_case = getallcase();
        for (var i = 0; i < list_all_case.length; i++) {
            if ((list_all_case[i].type) === "out") {
                if (($('#' + list_all_case[i].idcase).val()) !== "")
                {
                    var idm = getmain(list_all_case[i].idpart);
                    var idp = list_all_case[i].idpart;
                    var idc = list_all_case[i].idcase;
                    var val = ($('#' + list_all_case[i].idcase).val());
                    insertfunction(idfile, idm, idp, idc, val);
                }
            }
             else if (((list_all_case[i].idcase) !== "table") && (list_all_case[i].idcase) !== "out") {
                if ($('#' + list_all_case[i].idcase).is(':checked')) {

                    var idm = getmain(list_all_case[i].idpart);
                    var idp = list_all_case[i].idpart;
                    var idc = list_all_case[i].idcase;
                    insertfunction(idfile, idm, idp, idc, "empty");
                }
            }
        }
          var list_all_part = getallpart();
        for (var j = 0; j < list_all_part.length; j++) {
            if ($('#' + list_all_part[j].idpart).is(':checked')) {

                var idm = getmain(list_all_part[j].idpart);
                var idp = list_all_part[j].idpart;
                var idc="000";
                insertfunction(idfile, idm, idp, idc, "empty");
               
            }
        }
    }

    function  insertfunction(idfile, idmain, idpart, idcase, value)
    {
        insertfile(idfile, idmain, idpart, idcase, value);
    }

function setInputDate(_id) {
        var _dat = document.querySelector(_id);
        var hoy = new Date(),
                d = hoy.getDate(),
                m = hoy.getMonth() + 1,
                y = hoy.getFullYear(),
                data;

        if (d < 10) {
            d = "0" + d;
        }
        ;
        if (m < 10) {
            m = "0" + m;
        }
        ;

        data = y + "-" + m + "-" + d;
        console.log(data);
        _dat.value = data;
    }
function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    